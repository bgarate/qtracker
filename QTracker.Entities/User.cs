﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QTracker.Entities
{
    public class User
    {
        public User()
        {
            Roles = new List<UserRole>();
        }

        public User(string name)
        {
            Name = name;
            Roles = new List<UserRole>();
        }

        public User(Guid userId, string name)
        {
            UserId = userId;
            Name = name;
            Roles = new List<UserRole>();
        }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public List<TimeTrack> TimeTracks { get; set; }
        public List<UserRole> Roles { get; set; }
        public List<CustomerUserAccess> Mantains { get; set; }

        public void AddRole(Role role)
        {
            Roles.Add(new UserRole(this, role));
        }
    }
}
