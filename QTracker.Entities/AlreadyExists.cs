﻿using System;

namespace QTracker.Entities
{
    public class AlreadyExists : Exception
    {
        public AlreadyExists()
        {

        }

        public AlreadyExists(string message) : base(message)
        {

        }
    }
}