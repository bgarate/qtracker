﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QTracker.Entities
{
    public class Task
    {

        public Guid TaskId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Guid ProjectId { get; set; }

        public List<TimeTrack> TimeTracks { get; set; }
        public Project Project { get; set; }
        public Task()
        {

        }

        public Task(Project project, string description)
        {
            ProjectId = project.ProjectId;
            Description = description;
        }
    }
}