﻿namespace QTracker.Entities
{
    public class LogInformation
    {
        public LogInformation(string user, string password)
        {
            User = user;
            Password = password;
        }

        public string User { get; }
        public string Password { get; }
       
    }
}