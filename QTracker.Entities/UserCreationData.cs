﻿using System.ComponentModel.DataAnnotations;

namespace QTracker.Entities
{
    public class UserCreationData
    {
        // TODO: Make properties get-only

        public UserCreationData()
        {

        }

        public UserCreationData(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }

    }
}