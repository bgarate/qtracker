﻿using System;

namespace QTracker.Entities
{
    public class LoggedUser:IEquatable<LoggedUser>
    {
        public LoggedUser(Guid userId, Token token, DateTime? loggedOn, DateTime? loggedUntil)
        {
            UserId = userId;
            Token = token;
            LoggedOn = loggedOn;
            LoggedUntil = loggedUntil;
        }

        public Guid UserId { get; private set; }
        public Token Token { get; private set; }
        public DateTime? LoggedOn { get; private set; }
        public DateTime? LoggedUntil { get; private set; }

        public static LoggedUser NotLogged(Guid userId) => new LoggedUser(userId, Token.InvalidToken, null, null);


        public bool Equals(LoggedUser other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return UserId.Equals(other.UserId) && Equals(Token, other.Token) && LoggedOn.Equals(other.LoggedOn) && LoggedUntil.Equals(other.LoggedUntil);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LoggedUser) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = UserId.GetHashCode();
                hashCode = (hashCode * 397) ^ (Token != null ? Token.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LoggedOn.GetHashCode();
                hashCode = (hashCode * 397) ^ LoggedUntil.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(LoggedUser left, LoggedUser right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LoggedUser left, LoggedUser right)
        {
            return !Equals(left, right);
        }
    }
}