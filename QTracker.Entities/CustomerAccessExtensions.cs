﻿namespace QTracker.Entities
{
    public static class CustomerAccessExtensions
    {
        public static bool Can(this CustomerAccess actualLevel, CustomerAccess requestedLevel)
        {
            return (int) actualLevel >= (int) requestedLevel;
        }
    }
}