﻿using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace QTracker.Entities
{
    public class Token : IEquatable<Token>
    {
        private const string INVALID_TOKEN_VALUE = null;
        private const int TOKEN_SIZE_IN_BYTES = 256;

        public Token(string value)
        {
            if (value != null)
                Value = value.ToLower();

            if (!IsValid())
                Value = null;
        }

        public static Token InvalidToken => new Token(INVALID_TOKEN_VALUE);

        public bool IsValid()
        {
            Regex regex = new Regex(@"^[a-zA-Za-z0-9\=\+\/]*$", RegexOptions.Compiled);

            if (Value == null || Value == INVALID_TOKEN_VALUE)
                return false;

            return regex.IsMatch(Value);
        }

        public string Value { get; }

        public static Token Generate()
        {
            byte[] tokenBytes = new byte[TOKEN_SIZE_IN_BYTES / Consts.BITS_IN_BYTE];
            using (RandomNumberGenerator rnd = RandomNumberGenerator.Create())
            {
                rnd.GetBytes(tokenBytes);
            }

            string value = Convert.ToBase64String(tokenBytes);

            return new Token(value);
        }

        public override string ToString()
        {
            if (!IsValid())
                return "<Invalid token>";

            return Value;
        }

        public bool Equals(Token other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            if (!IsValid() || !other.IsValid())
                return true;

            return string.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Token) obj);
        }

        public override int GetHashCode()
        {
            return (!IsValid() ? Value.GetHashCode() : 0);
        }

        public static bool operator ==(Token left, Token right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Token left, Token right)
        {
            return !Equals(left, right);
        }
    }
}
