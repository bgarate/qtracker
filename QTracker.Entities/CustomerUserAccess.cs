﻿using System;

namespace QTracker.Entities
{
    public class CustomerUserAccess
    {
        public CustomerUserAccess()
        {

        }

        public CustomerUserAccess(Guid userId, Guid customerId, CustomerAccess access)
        {
            UserId = userId;
            CustomerId = customerId;
            Access = access;
        }

        public CustomerUserAccess(User user, Customer customer, CustomerAccess access)
        {
            User = user;
            UserId = user.UserId;
            Customer = customer;
            CustomerId = customer.CustomerId;
            Access = access;
        }

        public User User { get; set; }
        public Guid UserId { get; set; }

        public Customer Customer { get; set; }
        public Guid CustomerId { get; set; }

        public CustomerAccess Access { get; set; }

    }
}