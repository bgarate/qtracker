﻿using System;

namespace QTracker.Entities
{
    public class Role
    {
        public const string ADMIN_ROLE = "Admin";
        public const string USER_ROLE = "User";
        public static Role AdminRole => new Role(new Guid("00000000-0000-0000-0000-000000000001"), ADMIN_ROLE);
        public static Role UserRole => new Role(new Guid("00000000-0000-0000-0000-000000000002"), USER_ROLE);

        public Role(Guid roleId, string description)
        {
            RoleId = roleId;
            Description = description;
        }

        public Role()
        {

        }

        public Guid RoleId { get; private set; }

        public string Description { get; private set; }
    }
}