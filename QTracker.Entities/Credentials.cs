﻿using System;
using System.Collections.Generic;

namespace QTracker.Entities
{
    public class Credentials
    {
        public Credentials(Guid userId, string username, string saltedPassword)
        {
            UserId = userId;
            Username = username;
            SaltedPassword = saltedPassword;
        }

        public Credentials(string username, string saltedPassword)
        {
            UserId = Guid.Empty;
            Username = username;
            SaltedPassword = saltedPassword;
        }

        public  Guid UserId { get; private set; }
        public string SaltedPassword { get; private set; }
        public string Username { get; private set; }

    }
}