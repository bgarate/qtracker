﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QTracker.Entities
{
    public class Customer
    {

        public Customer()
        {
            
        }

        public Customer(string code, string name)
        {
            CustomerId = Guid.NewGuid();
            Name = name;
            Code = code;
        }

        public Guid CustomerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public List<Project> Projects { get; set; }

        public List<CustomerUserAccess> Mantainers { get; set; }
    }
}