﻿using System;

namespace QTracker.Entities
{
    public class UserRole
    {
        public UserRole()
        {

        }

        public UserRole(User user, Role role)
        {
            User = user;
            UserId = user.UserId;
            Role = role;
            RoleId = role.RoleId;
        }

        public UserRole(Guid userId, Guid roledId)
        {
            UserId = userId;
            RoleId = roledId;
        }

        public Guid UserId { get; private set; }
        public Guid RoleId { get; private set; }
        public User User { get; private set; }
        public Role Role { get; private set; }
    }
}