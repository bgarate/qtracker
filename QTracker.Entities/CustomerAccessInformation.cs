﻿using System;

namespace QTracker.Entities
{
    public class CustomerAccessInformation
    {
        public CustomerAccessInformation(Guid customerId, CustomerAccess access)
        {
            CustomerId = customerId;
            Access = access;
        }

        public Guid CustomerId { get;  }
        public CustomerAccess Access { get; }
    }
}