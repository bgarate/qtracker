﻿namespace QTracker.Entities
{
    public static class Policies
    {
        public const string CUSTOMER_MANTAINER = "CustomerUserMantainer";
        public const string CUSTOMER_ACCESS = "CustomerUserAccess";
        public const string OWN_USER_ACCESS = "OwnUserAccess";
    }
}