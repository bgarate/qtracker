﻿using System;

namespace QTracker.Entities
{
    public class NotExists : Exception
    {
        public NotExists()
        {

        }

        public NotExists(string message): base(message)
        {
            
        }
    }
}