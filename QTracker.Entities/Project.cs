﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QTracker.Entities
{
    public class Project
    {
        
        public Guid ProjectId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
        [Required]
        public string Name { get; set; }

        public List<Task> Tasks { get; set; }
        
        public Project()
        {

        }

        public Project(Customer customer, string name, string description)
        {
            CustomerId = customer.CustomerId;
            Name = name;
            Description = description;
        }
    }
}