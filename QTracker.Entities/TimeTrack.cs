﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QTracker.Entities
{
    public class TimeTrack
    {
        public Guid TimeTrackId { get; set; }
        [Required] public Guid UserId { get; set; }
        [Required] public Guid TaskId { get; set; }

        public Task Task { get; set; }
        [Required] public DateTime Start { get; set; }
        public DateTime? End { get; set; }

        public TimeTrack()
        {

        }

        public TimeTrack(Task task, User user, DateTime start, DateTime? end = null)
        {
            TaskId = task.TaskId;
            UserId = user.UserId;
            Start = start;
            End = end;
        }
    }
}