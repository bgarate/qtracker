﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;
using Task = QTracker.Entities.Task;

namespace QTracker.Services.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.USER_ROLE)]
    [ApiController]
    public class TimeTrackController : ControllerBase
    {
        public TimeTrackController(TimeTrackManager timeTrackManager)
        {
            TimeTrackManager = timeTrackManager;
        }

        private TimeTrackManager TimeTrackManager { get; }

        [HttpGet("{timeTrackId}")]
        [Authorize(Policy = Policies.CUSTOMER_ACCESS)]
        [ContentFilter]
        public ActionResult<TimeTrack> Get(Guid timeTrackId)
        {
            return TimeTrackManager.GetTimeTrack(timeTrackId);
        }

        [HttpPost]
        [Authorize(Policy = Policies.CUSTOMER_ACCESS)]
        [Authorize(Policy = Policies.OWN_USER_ACCESS)]
        public ActionResult Post([FromBody] TimeTrack timeTrack)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            TimeTrack createdTimeTrack = TimeTrackManager.AddTimeTrack(timeTrack);
            return Created(createdTimeTrack.TimeTrackId.ToString(), createdTimeTrack);


        }

    }

}
