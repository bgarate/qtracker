﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;

namespace QTracker.Services.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private LogManager LogManager { get; }

        public LogController(LogManager logManager)
        {
            LogManager = logManager;
        }

        [HttpPost]
        public ActionResult<Token> Post([FromBody]LogInformation information)
        {
            return LogManager.LogIn(information);
        }
        
    }
}
