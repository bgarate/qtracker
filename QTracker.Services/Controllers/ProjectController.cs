﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;

namespace QTracker.Services.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.USER_ROLE)]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        public ProjectController(ProjectManager projectManager)
        {
            ProjectManager = projectManager;
        }

        private ProjectManager ProjectManager { get; }

        [HttpGet("{projectId}")]
        [ContentFilter]
        public ActionResult<Project> Get(Guid projectId)
        {
            return ProjectManager.GetProject(projectId);
        }

        [HttpPost]
        [Authorize(Policy = Policies.CUSTOMER_MANTAINER)]
        public ActionResult Post([FromBody] Project project)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Project createdProject = ProjectManager.AddProject(project);
            return Created(createdProject.ProjectId.ToString(), project);

        }

    }
}
