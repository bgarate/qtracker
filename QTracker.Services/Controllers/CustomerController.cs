﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;

namespace QTracker.Services.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.USER_ROLE)]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        public CustomerController(CustomerManager customerManager)
        {
            CustomerManager = customerManager;
        }

        private CustomerManager CustomerManager { get; }

        [HttpGet]
        [ContentFilter]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            return CustomerManager.GetCustomers();
        }

        [HttpGet("{codeOrId}")]
        [ContentFilter]
        public ActionResult<Customer> Get(string codeOrId)
        {
            bool validGuid = Guid.TryParse(codeOrId, out Guid customerId);

            if (validGuid)
                return CustomerManager.GetCustomer(customerId);

            return CustomerManager.GetCustomer(codeOrId);
        }


        [HttpPost]
        [Authorize(Roles = Role.ADMIN_ROLE)]
        public ActionResult Post([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Customer createdCustomer = CustomerManager.AddCustomer(customer);
            return Created(createdCustomer.CustomerId.ToString(), customer);

        }

    }
}
