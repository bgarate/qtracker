﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;
using Task = QTracker.Entities.Task;

namespace QTracker.Services.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.USER_ROLE)]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(UserManager userManager)
        {
            UserManager = userManager;
        }

        private UserManager UserManager { get; }

        [HttpGet("{userIdOrLogin}")]
        [Authorize(Policy = Policies.OWN_USER_ACCESS)]
        public ActionResult<User> Get(string userIdOrLogin)
        {
            bool validGuid = Guid.TryParse(userIdOrLogin, out Guid userId);

            if (validGuid)
                return UserManager.GetUser(userId);

            return UserManager.GetUser(userIdOrLogin);
        }

        [HttpGet("{userId}/tracking")]
        [Authorize(Policy = Policies.OWN_USER_ACCESS)]
        public ActionResult<IEnumerable<Customer>> Get([FromQuery] DateTime start, [FromQuery] DateTime end,
            Guid userId)
        {
            return UserManager.GetCustomers(start, end, userId);
        }

        [HttpPost]
        [Authorize(Roles = Role.ADMIN_ROLE)]
        public ActionResult Post([FromBody] UserCreationData userCreationData)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            User createdUser = UserManager.AddUser(userCreationData);
            return Created(createdUser.UserId.ToString(), createdUser);


        }

        [HttpPost("{userId}/accesses")]
        [Authorize(Roles = Role.ADMIN_ROLE)]
        public ActionResult Accesses(Guid userId, [FromBody] CustomerAccessInformation accessInformation)
        {

            CustomerUserAccess userAccess = UserManager.Accesses(userId, accessInformation);
            return Created(userAccess.UserId.ToString(), userAccess);

        }

        [HttpPatch("{userId}/accesses")]
        [Authorize(Roles = Role.ADMIN_ROLE)]
        public ActionResult ChangeAccesses(Guid userId, [FromBody] CustomerAccessInformation accessInformation)
        {

            CustomerUserAccess userAccess = UserManager.ChangeAccesses(userId, accessInformation);
            return Ok(userAccess);

        }

    }
}
