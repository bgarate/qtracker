﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QTracker.BusinessLogic;
using QTracker.Entities;
using Task = QTracker.Entities.Task;

namespace QTracker.Services.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.USER_ROLE)]
    [ApiController]
    public class TaskController : ControllerBase
    {
        public TaskController(TaskManager taskManager)
        {
            TaskManager = taskManager;
        }

        private TaskManager TaskManager { get; }

        [HttpGet("{taskId}")]
        [Authorize(Policy = Policies.CUSTOMER_ACCESS)]
        [ContentFilter]
        public ActionResult<Task> Get(Guid taskId)
        {
            return TaskManager.GetTask(taskId);
        }

        [HttpPost]
        [Authorize(Policy = Policies.CUSTOMER_ACCESS)]
        public ActionResult Post([FromBody] Task task)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Task createdTask = TaskManager.AddTask(task);
            return Created(createdTask.TaskId.ToString(), createdTask);

        }

    }
}
