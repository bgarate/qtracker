﻿using Autofac;
using QTracker.BusinessLogic;
using QTracker.DataAccess;

namespace QTracker.Services
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Authenticator>();
            builder.RegisterType<DateTimeProvider>().As<IDateTimeProvider>();
            
            builder.RegisterType<CustomerDataAccess>();
            builder.RegisterType<AuthDataAccess>();
            builder.RegisterType<ProjectDataAccess>();
            builder.RegisterType<TaskDataAccess>();
            builder.RegisterType<UserDataAccess>();
            builder.RegisterType<TimeTrackDataAccess>();

            builder.RegisterType<LogManager>();
            builder.RegisterType<CustomerManager>();
            builder.RegisterType<ProjectManager>();
            builder.RegisterType<TaskManager>();
            builder.RegisterType<UserManager>();
            builder.RegisterType<TimeTrackManager>();
        }
    }
}