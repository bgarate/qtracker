﻿using System;
using System.Linq;
using System.Security.Claims;
using QTracker.Entities;

namespace QTracker.Services
{
    public static class ClaimsExtensions
    {
        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            return user.Claims.Any(c => c.Type == ClaimTypes.Role && c.Value == Role.ADMIN_ROLE);
        }

        public static Guid? UserId(this ClaimsPrincipal user)
        {
            Claim claim = user.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            return claim != null ? new Guid(claim.Value) : (Guid?)null;
        }
        
    }
}