﻿using System;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using QTracker.BusinessLogic;
using QTracker.Entities;
using Task = System.Threading.Tasks.Task;

namespace QTracker.Services
{
    public class ExceptionCatchFilterAttribute : ExceptionFilterAttribute
    {

     
        public override void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            string message = "An error has ocurred";

            Exception ex = context.Exception;
            if (ex is AlreadyExists)
            {
                status = HttpStatusCode.Conflict;
                message = ex.Message;
            }
            else if (ex is NotExists)
            {
                status = HttpStatusCode.BadRequest;
                message = ex.Message;
            }
            else if (ex is ArgumentException)
            {
                status = HttpStatusCode.BadRequest;
                message = ex.Message;
            }

            context.ExceptionHandled = true;
            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)status;
            response.ContentType = "application/json";
            context.Result = new ObjectResult(new
            {
                Message = message
            });
        }
    }
}