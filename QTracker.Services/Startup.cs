﻿using System.Net.Http;
using System.Threading;
using Autofac;
using Autofac.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            DefaultContractResolver contractResolver = new CamelCasePropertyNamesContractResolver();

            services.AddMvc(config=> {
                config.Filters.Add(typeof(ExceptionCatchFilterAttribute));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddOptions();
            services.Configure<MvcJsonOptions>(config =>
            {
                config.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                config.SerializerSettings.ContractResolver = contractResolver;
            });
            services.AddDbContext<QTrackerContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("QTracker"));
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = TokenAuthenticationOptions.DEFAULT_SCHEME;
                options.DefaultChallengeScheme = TokenAuthenticationOptions.DEFAULT_SCHEME;
            }).AddTokenSupport(options => { });
            services.AddAuthorization(options =>
                {
                    options.AddPolicy(Policies.CUSTOMER_MANTAINER,
                        p => p.Requirements.Add(new CustomerAccessRequirement(CustomerAccess.Mantainer)));
                    options.AddPolicy(Policies.CUSTOMER_ACCESS,
                        p => p.Requirements.Add(new CustomerAccessRequirement(CustomerAccess.Access)));
                    options.AddPolicy(Policies.OWN_USER_ACCESS,
                        p => p.Requirements.Add(new OwnUserAccessRequirement()));
                });

            services.AddSingleton<IAuthorizationHandler, CustomerAccessHandler>();
            services.AddSingleton<IAuthorizationHandler, OwnUserAccessHandler>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<QTrackerContext>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }
    }
}


