﻿using Microsoft.AspNetCore.Mvc;

namespace QTracker.Services
{
    public class ContentFilterAttribute : TypeFilterAttribute
    {

        public ContentFilterAttribute() : base(typeof(ContentFilter))
        {

        }


    }
}