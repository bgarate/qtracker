﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.Services
{
    public class ContentFilter : IActionFilter
    {
        private readonly UserDataAccess userDataAccess;

        public ContentFilter(UserDataAccess userDataAccess)
        {
            this.userDataAccess = userDataAccess;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {

        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.HttpContext.User.IsAdmin())
            {
                return;
            }

            Claim userIdClaim =
                context.HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (userIdClaim != null)
            {

                Guid userId = new Guid(userIdClaim.Value);
                User user = userDataAccess.GetUser(userId);

                ObjectResult result = context.Result as ObjectResult;
                
                if (result?.Value is IEnumerable<Customer> customers)
                {
                    List<Customer> filteredCustomers =
                        customers.Where(c => user.Mantains.Any(m => m.CustomerId == c.CustomerId)).ToList();

                    result.Value = filteredCustomers;

                } else if (result?.Value is Customer customer)
                {
                    if(user.Mantains.All(m=>m.CustomerId != customer.CustomerId))
                        context.Result = new ForbidResult();

                } else if (result?.Value is IEnumerable<Project> projects)
                {
                    List<Project> filteredProjects =
                        projects.Where(p => user.Mantains.Any(m => m.CustomerId == p.CustomerId)).ToList();

                    result.Value = filteredProjects;
                }
                else if (result?.Value is Project project)
                {
                    if (user.Mantains.All(m => m.CustomerId != project.CustomerId))
                        context.Result = new ForbidResult();

                }
                else if (result?.Value is IEnumerable<Task> tasks)
                {
                    List<Task> filteredTasks =
                        tasks.Where(t => user.Mantains.Any(m => m.CustomerId == t.Project.CustomerId)).ToList();

                    result.Value = filteredTasks;
                }
                else if (result?.Value is Task task)
                {
                    if (user.Mantains.All(m => m.CustomerId != task.Project.CustomerId))
                        context.Result = new ForbidResult();
                }
                else if (result?.Value is IEnumerable<TimeTrack> timeTracks)
                {
                    List<TimeTrack> filteredTimeTracks =
                        timeTracks.Where(t => CanAccessTimeTrack(t, user.UserId, user.Mantains)).ToList();

                    result.Value = filteredTimeTracks;
                }
                else if (result?.Value is TimeTrack timeTrack)
                {
                    if (!CanAccessTimeTrack(timeTrack, user.UserId, user.Mantains))
                        context.Result = new ForbidResult();
                }
            }
            else
            {
                context.Result = new ForbidResult();
            }
        }

        private bool CanAccessTimeTrack(TimeTrack timeTrack, Guid userId, List<CustomerUserAccess> accesses)
        {
            foreach (CustomerUserAccess access in accesses)
            {
                if(timeTrack.Task.Project.CustomerId != access.CustomerId)
                    continue;

                if (access.Access == CustomerAccess.Mantainer || userId == timeTrack.UserId)
                    return true;
            }

            return false;
        }
    }
}