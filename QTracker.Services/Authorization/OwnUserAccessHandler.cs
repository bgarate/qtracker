﻿using System;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QTracker.BusinessLogic;
using QTracker.DataAccess;
using QTracker.Entities;
using Task = System.Threading.Tasks.Task;

namespace QTracker.Services
{
    public class OwnUserAccessHandler : AuthorizationHandler<OwnUserAccessRequirement>
    {
        private readonly UserManager userManager;
        private readonly UserDataAccess userDataAccess;
        private readonly ProjectDataAccess projectDataAccess;
        private readonly CustomerDataAccess customerDataAccess;
        private readonly TaskDataAccess taskDataAccess;

        public OwnUserAccessHandler(UserDataAccess userDataAccess, CustomerDataAccess customerDataAccess,
            ProjectDataAccess projectDataAccess, TaskDataAccess taskDataAccess, UserManager userManager)
        {
            this.customerDataAccess = customerDataAccess;
            this.projectDataAccess = projectDataAccess;
            this.taskDataAccess = taskDataAccess;
            this.userManager = userManager;
            this.userDataAccess = userDataAccess;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OwnUserAccessRequirement requirement)
        {

            if (context.User.IsAdmin())
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            if (context.Resource is AuthorizationFilterContext authContext)
            {
                HttpContext httpContext = authContext.HttpContext;
                JObject body = null;

                httpContext.Request.EnableRewind();
                StreamReader streamReader = new StreamReader(httpContext.Request.Body);

                string strBody = streamReader.ReadToEnd();
                try
                {
                    body = JObject.Parse(strBody);
                }
                catch (JsonReaderException)
                {
                }


                httpContext.Request.Body.Seek(0, SeekOrigin.Begin);


                Guid? sentUserId = null; 
                if (authContext.RouteData.Values.TryGetValue("userId", out object objUserId))
                {
                    sentUserId = new Guid((string)objUserId);
                }
                else if (body != null && body.ContainsKey("userId"))
                {
                    sentUserId = new Guid(body["userId"].Value<string>());
                }
                else if (authContext.RouteData.Values.TryGetValue("userIdOrLogin", out object userIdOrLogin))
                {

                    bool validGuid = Guid.TryParse((string) userIdOrLogin, out Guid validUserId);

                    if (validGuid)
                    {
                        sentUserId = validUserId;
                    } else {
                        User user = userManager.GetUser((string) userIdOrLogin);
                        if (user != null)
                            sentUserId = user.UserId;
                    }
                }
                else
                {
                    context.Fail();
                    return Task.CompletedTask;
                }

                Guid? loggedUserId = context.User.UserId();

                if (sentUserId != null && loggedUserId == sentUserId)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }
            }

            return Task.CompletedTask;
        }

    }
}