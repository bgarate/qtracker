﻿using Microsoft.AspNetCore.Authorization;
using QTracker.Entities;

namespace QTracker.Services
{
    public class CustomerAccessRequirement : IAuthorizationRequirement
    {
        public CustomerAccessRequirement(CustomerAccess accessLevel)
        {
            AccessLevel = accessLevel;
        }

        public CustomerAccess AccessLevel { get;  }
    }
}