﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QTracker.DataAccess;
using QTracker.Entities;
using Task = System.Threading.Tasks.Task;

namespace QTracker.Services
{
    public class CustomerAccessHandler : AuthorizationHandler<CustomerAccessRequirement>
    {
        private readonly UserDataAccess userDataAccess;
        private readonly ProjectDataAccess projectDataAccess;
        private readonly TaskDataAccess taskDataAccess;
        private readonly TimeTrackDataAccess timeTrackDataAccess;

        public CustomerAccessHandler(UserDataAccess userDataAccess, ProjectDataAccess projectDataAccess,
            TaskDataAccess taskDataAccess, TimeTrackDataAccess timeTrackDataAccess)
        {
            this.projectDataAccess = projectDataAccess;
            this.taskDataAccess = taskDataAccess;
            this.timeTrackDataAccess = timeTrackDataAccess;
            this.userDataAccess = userDataAccess;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomerAccessRequirement requirement)
        {
            
            if (context.User.IsAdmin())
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }
            
            if (context.Resource is AuthorizationFilterContext authContext)
            {
                HttpContext httpContext = authContext.HttpContext;
                JObject body = null;

                httpContext.Request.EnableRewind();
                StreamReader streamReader = new StreamReader(httpContext.Request.Body);

                string strBody = streamReader.ReadToEnd();
                try
                {
                    body = JObject.Parse(strBody);
                }
                catch (JsonReaderException)
                {
                }
                

                httpContext.Request.Body.Seek( 0,SeekOrigin.Begin);
                

                Guid customerId;
                if (authContext.RouteData.Values.TryGetValue("customerId", out object objCustomerId))
                {
                    customerId = new Guid((string) objCustomerId);
                }
                else if (body != null && body.ContainsKey("customerId"))
                {
                    customerId = new Guid(body["customerId"].Value<string>());
                }
                else if (authContext.RouteData.Values.TryGetValue("projectId", out object objProjectId))
                {
                    Guid projectId = new Guid((string)objProjectId);
                    customerId = GetCustomerIdFromProjectId(projectId);
                }
                else if (body != null && body.ContainsKey("projectId"))
                {
                    Guid projectId = new Guid(body["projectId"].Value<string>());
                    customerId = GetCustomerIdFromProjectId(projectId);
                }
                else if (authContext.RouteData.Values.TryGetValue("taskId", out object objTaskId))
                {
                    Guid taskId = new Guid((string) objTaskId);
                    customerId = GetCustomerIdFromTaskId(taskId);
                }
                else if (body != null && body.ContainsKey("taskId"))
                {
                    Guid taskId = new Guid(body["taskId"].Value<string>());
                    customerId = GetCustomerIdFromTaskId(taskId);
                }
                else if (authContext.RouteData.Values.TryGetValue("timeTrackId", out object objTimeTrackId))
                {
                    Guid timeTrackId = new Guid((string)objTimeTrackId);
                    customerId = GetCustomerIdFromTimeTrackId(timeTrackId);
                }
                else if (body != null && body.ContainsKey("timeTrackId"))
                {
                    Guid timeTrackId = new Guid(body["timeTrackId"].Value<string>());
                    customerId = GetCustomerIdFromTimeTrackId(timeTrackId);
                }
                else
                {
                    context.Fail();
                    return Task.CompletedTask;
                }

                Guid? userId = context.User.UserId();
                if (userId != null)
                {
                    User user = userDataAccess.GetUser(userId.Value);

                    if (user.Mantains.Any(mc => mc.CustomerId == customerId && mc.Access.Can(requirement.AccessLevel)))
                    {
                        context.Succeed(requirement);
                    }
                    else
                    {
                        context.Fail();
                    }
                }
                else
                {
                    context.Fail();
                }
            }

            return Task.CompletedTask;
        }

        private Guid GetCustomerIdFromTaskId(Guid taskId)
        {
            Entities.Task task = taskDataAccess.GetTask(taskId, true);
            Guid customerId = task.Project.CustomerId;
            return customerId;
        }

        private Guid GetCustomerIdFromTimeTrackId(Guid timeTrackId)
        {
            Entities.TimeTrack timeTrack = timeTrackDataAccess.GetTimeTrack(timeTrackId);
            Guid customerId = timeTrack.Task.Project.CustomerId;
            return customerId;
        }

        private Guid GetCustomerIdFromProjectId(Guid projectId)
        {
            Project project = projectDataAccess.GetProject(projectId);
            return project.CustomerId;
        }
    }
}