﻿using Microsoft.AspNetCore.Authentication;

namespace QTracker.Services
{
    public class TokenAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DEFAULT_SCHEME = "Token";
        public string Scheme => DEFAULT_SCHEME;
        public string AuthenticationType = DEFAULT_SCHEME;
    }
}