﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using QTracker.BusinessLogic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.Services
{
    public class TokenAuthenticationHandler : AuthenticationHandler<TokenAuthenticationOptions>
    {
        private readonly AuthDataAccess authDataAccess;
        private readonly Authenticator authenticator;

        public TokenAuthenticationHandler(IOptionsMonitor<TokenAuthenticationOptions> options, ILoggerFactory logger,
            UrlEncoder encoder, ISystemClock clock, AuthDataAccess authDataAccess, Authenticator authenticator) : base(options, logger, encoder, clock)
        {
            this.authDataAccess = authDataAccess;
            this.authenticator = authenticator;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            bool hasAuthorizationHeader = Request.Headers.TryGetValue("Authorization", out StringValues values);

            if (!hasAuthorizationHeader)
                return AuthenticateResult.NoResult();

            string strToken = values.Single().Split(" ").Last();
            Token token = new Token(strToken);
            
            if (!token.IsValid())
                return AuthenticateResult.NoResult();
            
            LoggedUser loggedUser = authDataAccess.GetLoggedUser(token);

            if (!authenticator.IsLogged(loggedUser))
                return AuthenticateResult.NoResult();

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, loggedUser.UserId.ToString()),
                new Claim(ClaimTypes.NameIdentifier, loggedUser.UserId.ToString())
            };

            List<Role> roles = authDataAccess.GetRoles(loggedUser.UserId);
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role.Description)));

            ClaimsIdentity identity = new ClaimsIdentity(claims, Options.AuthenticationType);
            List<ClaimsIdentity> identities = new List<ClaimsIdentity> { identity };
            ClaimsPrincipal principal = new ClaimsPrincipal(identities);
            AuthenticationTicket ticket = new AuthenticationTicket(principal, Options.Scheme);

            return AuthenticateResult.Success(ticket);


        }
    }
}