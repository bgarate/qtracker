﻿using System;
using Microsoft.AspNetCore.Authentication;

namespace QTracker.Services
{
    public static class TokenAuthenticationScheme
    {
        public static AuthenticationBuilder AddTokenSupport(this AuthenticationBuilder authenticationBuilder,
            Action<TokenAuthenticationOptions> options)
        {
            return authenticationBuilder.AddScheme<TokenAuthenticationOptions, TokenAuthenticationHandler>(
                TokenAuthenticationOptions.DEFAULT_SCHEME, options);
        }
    }
}