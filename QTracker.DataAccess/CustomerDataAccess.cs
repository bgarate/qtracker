﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class CustomerDataAccess
    {
        public CustomerDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }

        public List<Customer> GetCustomers()
        {
            List<Customer> customers = Ctx.Customers.ToList();

            return customers;

        }

        public Customer GetCustomer(Guid customerId)
        {
            Customer customer = Ctx.Customers
                .Include(c=>c.Projects)
                .Include(c=>c.Mantainers)
                .Single(c=>c.CustomerId == customerId);
            
            return customer;

        }

        public Customer GetCustomer(string code)
        {
            Customer customer = Ctx.Customers
                .Include(c => c.Projects)
                .Include(c => c.Mantainers)
                .Single(c=>string.Equals(c.Code, code, StringComparison.CurrentCultureIgnoreCase));
            
            return customer;

        }

        public Customer AddCustomer(Customer customer)
        {
            customer.CustomerId = Guid.NewGuid();

            customer.Code = customer.Code.ToUpper();

            List<Customer> matchingCustomers =
                Ctx.Customers.Where(c => c.Code.ToUpper() == customer.Code.ToUpper()).ToList();

            if(matchingCustomers.Any())
                throw new AlreadyExists();

            Ctx.Customers.Add(customer);

            Ctx.SaveChanges();

            return customer;
        }
    }
}