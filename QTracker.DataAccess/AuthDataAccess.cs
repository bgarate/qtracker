﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class AuthDataAccess
    {
        public AuthDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }

        public virtual Credentials GetCredentials(string username)
        {

            string sanitizedUsername = SanitizeUsername(username);
            Credentials credentials = Ctx.Credentials.SingleOrDefault(c => c.Username == sanitizedUsername);

            return credentials;

        }

        public virtual List<Role> GetRoles(Guid userId)
        {
            List<UserRole> userRoles = Ctx.UserRoles.Where(ur => ur.UserId == userId).Include(ur => ur.Role).ToList();

            return userRoles.Select(ur => ur.Role).ToList();

        }

        public virtual LoggedUser GetLoggedUser(Token token)
        {
            LoggedUser loggedUser = Ctx.LoggedUsers.SingleOrDefault(l => l.Token.Value == token.Value);

            return loggedUser;

        }

        public virtual void LogUser(LoggedUser log)
        {

            LoggedUser loggedUserDb = Ctx.LoggedUsers.SingleOrDefault(l => l.UserId == log.UserId);

            bool exists = loggedUserDb != null;

            if (exists)
                Ctx.Entry(loggedUserDb).State = EntityState.Detached;

            Ctx.Attach(log);
            Ctx.Entry(log).State = exists ? EntityState.Modified : EntityState.Added;
            Ctx.SaveChanges();
            ;
        }

        public virtual void AddCredentials(User user, Credentials credentials)
        {
            credentials = new Credentials(user.UserId, SanitizeUsername(credentials.Username), credentials.SaltedPassword);
            Ctx.Credentials.Add(credentials);
            Ctx.SaveChanges();
        }

        private string SanitizeUsername(string username)
        {
            return username.Trim().ToLower();
        }
    }
}
