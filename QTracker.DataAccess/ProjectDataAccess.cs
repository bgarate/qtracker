﻿using System;
using System.Collections.Generic;
using System.Linq;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class ProjectDataAccess
    {
        public ProjectDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }

        public Project GetProject(Guid projectId)
        {
            Project project = Ctx.Projects.Find(projectId);
            Ctx.Entry(project).Collection(p=>p.Tasks).Load();
            return project;

        }

        public Project AddProject(Project project)
        {
            project.ProjectId = Guid.NewGuid();

            Customer customerDb = Ctx.Customers.Find(project.CustomerId);

            if (customerDb == null)
                throw new NotExists();

            List<Project> matchingProjects =
                Ctx.Projects
                    .Where(p => p.Name.ToUpper() == project.Name.ToUpper() && p.CustomerId == project.CustomerId)
                    .ToList();

            if(matchingProjects.Any())
                throw new AlreadyExists();

            Ctx.Projects.Add(project);

            Ctx.SaveChanges();

            return project;
        }
    }
}