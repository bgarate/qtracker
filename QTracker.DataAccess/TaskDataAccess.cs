﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class TaskDataAccess
    {
        public TaskDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }

        public Task GetTask(Guid taskId, bool loadProject = false)
        {
            Task task = Ctx.Tasks.Find(taskId);

            if(loadProject)
                Ctx.Entry(task).Reference(t=>t.Project).Load();

            return task;

        }


        public Task AddTask(Task task)
        {
            task.TaskId = Guid.NewGuid();

            Project projectDb = Ctx.Projects.Find(task.ProjectId);

            if (projectDb == null)
                throw new NotExists();

            Ctx.Tasks.Add(task);

            Ctx.SaveChanges();

            return task;
        }

    }
}