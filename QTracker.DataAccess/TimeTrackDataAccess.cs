﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class TimeTrackDataAccess
    {
        public TimeTrackDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }
        
        public TimeTrack AddTimeTrack(TimeTrack track)
        {
            track.TimeTrackId = Guid.NewGuid();

            User user = Ctx.Users.Find(track.UserId);

            if(user == null)
                throw new NotExists($"User with id {track.UserId} does not exists");

            Task task = Ctx.Tasks.Find(track.TaskId);

            if (task == null)
                throw new NotExists($"Task with id {track.TaskId} does not exists");

            Ctx.TimeTracks.Add(track);

            Ctx.SaveChanges();

            return track;
        }

        public TimeTrack GetTimeTrack(Guid timeTrackId)
        {
            return Ctx.TimeTracks.Include(t => t.Task.Project.Customer).Single(t => t.TimeTrackId == timeTrackId);
        }
    }
}