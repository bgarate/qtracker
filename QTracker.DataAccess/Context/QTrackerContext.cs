﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class QTrackerContext :DbContext
    {
        public DbSet<Credentials> Credentials { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<LoggedUser> LoggedUsers { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TimeTrack> TimeTracks { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<CustomerUserAccess> CustomerAccesses { get; set; }

        public QTrackerContext(DbContextOptions<QTrackerContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ValueConverter<Token, string> tokenConverter = new ValueConverter<Token, string>(token => token.ToString(), v => new Token(v));

            modelBuilder.Entity<LoggedUser>().HasKey(l=>l.UserId);
            modelBuilder.Entity<LoggedUser>().Property(l => l.Token).HasConversion(tokenConverter);

            modelBuilder.Entity<Credentials>().HasKey(c => c.UserId);

            modelBuilder.Entity<UserRole>().HasKey(ur => new {ur.UserId, ur.RoleId});
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.User).WithMany(u => u.Roles).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.Role).WithMany().HasForeignKey(r => r.RoleId);

            modelBuilder.Entity<CustomerUserAccess>().HasKey(ur => new { ur.UserId, ur.CustomerId });
            modelBuilder.Entity<CustomerUserAccess>().HasOne(ur => ur.User).WithMany(u => u.Mantains)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<CustomerUserAccess>().HasOne(ur => ur.Customer).WithMany(c => c.Mantainers)
                .HasForeignKey(r => r.CustomerId);

            // TODO: Setup to guarantee FK UserId in credentials and LoggedUser

            modelBuilder.Entity<Project>()
                .HasOne(p=>p.Customer)
                .WithMany(c => c.Projects)
                .HasForeignKey(p => p.CustomerId)
                .IsRequired();

            modelBuilder.Entity<Task>()
                .HasOne(t=>t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .IsRequired();

            modelBuilder.Entity<TimeTrack>()
                .HasOne<User>()
                .WithMany(u => u.TimeTracks)
                .HasForeignKey(t => t.UserId)
                .IsRequired();

            modelBuilder.Entity<TimeTrack>()
                .HasOne(t=>t.Task)
                .WithMany(u => u.TimeTracks)
                .HasForeignKey(t => t.TaskId)
                .IsRequired();
            
            Seed(modelBuilder);
        }

        private static void Seed(ModelBuilder modelBuilder)
        {
            Guid testUserId = new Guid("0FEF6B0D-77C3-470F-A40B-4EA783E7FE7D");

            modelBuilder.Entity<User>().HasData(new User(testUserId, "Test user"));

            modelBuilder.Entity<Credentials>()
                .HasData(new Credentials(testUserId, "testUser",
                    "1024." +
                    "uGvxvFAatKvyxDIrc1oCbqJ1V2cY9801d5YFp7HkPQex3zSi/fzKw5uNyOEL0K4LPjIHPcpyEma0SBd9wF/QIw==." +
                    "Q69+/+byJjxgGI4Q6vNYx8wnXJgx+ZoWRIbEamh1E3t3lKIUDdO3XB2QmP2uCmTHmd9O9HRhN+txjgr7cuG5OA=="));

            modelBuilder.Entity<Role>().HasData(Role.AdminRole, Role.UserRole);
            modelBuilder.Entity<UserRole>().HasData(new UserRole(testUserId, Role.UserRole.RoleId));
            modelBuilder.Entity<UserRole>().HasData(new UserRole(testUserId, Role.AdminRole.RoleId));

            modelBuilder.Entity<Customer>().HasData(new Customer("ITAUUY", "Banco Itaú Uruguay"));
            modelBuilder.Entity<Customer>().HasData(new Customer("ANCAP", "Ancap"));
            modelBuilder.Entity<Customer>().HasData(new Customer("MIDES", "Ministerio de Desarrollo Social"));
        }
    }
}
