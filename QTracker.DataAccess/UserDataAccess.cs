﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QTracker.Entities;

namespace QTracker.DataAccess
{
    public class UserDataAccess
    {
        public UserDataAccess(QTrackerContext ctx)
        {
            Ctx = ctx;
        }

        private QTrackerContext Ctx { get; }

        public virtual User GetUser(Guid userId)
        {
            User user = Ctx.Users
                .Include(u => u.Roles)
                .ThenInclude(ur => ur.Role)
                .Include(u => u.Mantains)
                .Single(u => u.UserId == userId);

            return user;
        }
        
        public virtual User AddUser(User user)
        {
            user.UserId = Guid.NewGuid();

            Ctx.Users.Add(user);

            Ctx.SaveChanges();

            return user;
        }

        public virtual CustomerUserAccess Accesses(Guid userId, Guid customerId, CustomerAccess access, bool updateIfExistent)
        {
            User user = Ctx.Users.SingleOrDefault(u => u.UserId == userId);

            if (user == null)
                throw new NotExists($"User {userId} does not exists");

            Customer customer = Ctx.Customers.SingleOrDefault(c => c.CustomerId == customerId);

            if (customer == null)
                throw new NotExists($"Customer {customerId} does not exists");

            CustomerUserAccess userAccess = Ctx.CustomerAccesses.SingleOrDefault(m => m.UserId == userId && m.CustomerId == customerId);

            if (userAccess == null && access == CustomerAccess.None)
                throw new ArgumentException($"Invalid access {access} selected", nameof(access));

            if (userAccess != null)
            {
                if (!updateIfExistent)
                    throw new AlreadyExists();

                userAccess.Access = access;
            }
            else
            {
                userAccess = new CustomerUserAccess(user, customer, access);

                Ctx.CustomerAccesses.Add(userAccess);

            }
            
            Ctx.SaveChanges();

            return userAccess;

        }

        public virtual List<Customer> GetCustomers(DateTime start, DateTime end, Guid userId)
        {
            List<TimeTrack> timeTracks = Ctx.TimeTracks
                .Where(t => t.Start > start && t.Start < end && t.UserId == userId)
                .Include(t => t.Task.Project.Customer).ToList();

            List<Customer> customers = timeTracks.Select(t => t.Task.Project.Customer).Distinct().ToList();
            foreach (Customer customer in customers)
            {
                customer.Projects = timeTracks.Select(t => t.Task.Project)
                    .Where(p => p.CustomerId == customer.CustomerId).Distinct().ToList();
                foreach (Project project in customer.Projects)
                {
                    project.Tasks = timeTracks.Select(t => t.Task).Where(t => t.ProjectId == project.ProjectId).Distinct().ToList();
                    foreach (Task task in project.Tasks)
                    {
                        task.TimeTracks = timeTracks.Where(t => t.TaskId == task.TaskId).ToList();
                    }
                }
            }
            
            return customers;
        }
    }
}