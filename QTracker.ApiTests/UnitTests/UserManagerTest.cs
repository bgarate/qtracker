﻿using System;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using QTracker.ApiTests.Mocks;
using QTracker.BusinessLogic;
using QTracker.DataAccess;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.UnitTests
{
    public class UserManagerTest
    {
        private Mock<IDateTimeProvider> nowTimeProviderMoq;
        private DateTime now;
        
        [OneTimeSetUp]
        public void Setup()
        {
            now = DateTime.Now;
            nowTimeProviderMoq = DateTimeProviderMock.ForDate(now);
            
        }
        [Test]
        public void IfUsernameAlreadyExistsShouldThrow()
        {
            Mock<UserDataAccess> userDataAccessMoq = UserDataAccessMockExtensions.GetMock();
            Mock<Authenticator> authenticatorMoq = AuthenticatorMockExtensions.GetMock(nowTimeProviderMoq.Object);
            Mock<AuthDataAccess> authDataAccessMoq = AuthDataAccessMockExtensions.GetMock();

            authDataAccessMoq.GetCredentialsReturnsCredentials();
            
            UserManager userManager = new UserManager(userDataAccessMoq.Object, authDataAccessMoq.Object,
                authenticatorMoq.Object);

            UserCreationData userCreationData = new UserCreationData("name","login","password");

            Should.Throw<AlreadyExists>(() => userManager.AddUser(userCreationData));

            authDataAccessMoq.Verify(m=>m.GetCredentials("login"),Times.Once);
        }

        [Test]
        public void NewUserShouldAddUserAndCredentials()
        {
            Mock<UserDataAccess> userDataAccessMoq = UserDataAccessMockExtensions.GetMock();
            Mock<Authenticator> authenticatorMoq = AuthenticatorMockExtensions.GetMock(nowTimeProviderMoq.Object);
            Mock<AuthDataAccess> authDataAccessMoq = AuthDataAccessMockExtensions.GetMock();

            userDataAccessMoq.AddUserReturnsUser();
            authDataAccessMoq.GetCredentialsReturnsNull();
            authenticatorMoq.GenerateCredentialsReturnsCredentials();

            UserManager userManager = new UserManager(userDataAccessMoq.Object, authDataAccessMoq.Object,
                authenticatorMoq.Object);

            UserCreationData userCreationData = new UserCreationData("name", "login", "password");
            User user = userManager.AddUser(userCreationData);

            user.Name.ShouldBe("name");

            authDataAccessMoq.Verify(m => m.GetCredentials("login"), Times.Once);
            authenticatorMoq.Verify(m => m.GenerateCredentials(userCreationData), Times.Once);
            userDataAccessMoq.Verify(m => m.AddUser(It.Is<User>(u => u.Name == "name")), Times.Once);
            authDataAccessMoq.Verify(
                m => m.AddCredentials(It.Is<User>(u => u.Name == "name"),
                    It.Is<Credentials>(c => c.Username == userCreationData.Login)), Times.Once);
        }

    }
}