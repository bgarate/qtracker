﻿using System;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using QTracker.ApiTests.Mocks;
using QTracker.BusinessLogic;
using QTracker.DataAccess;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.UnitTests
{
    public class LogManagerTest
    {
        private Mock<IDateTimeProvider> nowTimeProviderMoq;
        private DateTime now;
        
        [OneTimeSetUp]
        public void Setup()
        {
            now = DateTime.Now;
            nowTimeProviderMoq = DateTimeProviderMock.ForDate(now);
        }

        [Test]
        public void IfAuthenticationSucceedsShouldReturnValidTokenAndLogTheUser()
        {

            Mock<Authenticator> authenticatorMoq = AuthenticatorMockExtensions.GetMock(nowTimeProviderMoq.Object);
            Mock<AuthDataAccess> authDataAccessMoq = AuthDataAccessMockExtensions.GetMock();

            Credentials credentials = new Credentials(Guid.NewGuid(), "user", "saltedPassword");
            LoggedUser loggedUser = new LoggedUser(credentials.UserId, Token.Generate(), DateTime.Now, DateTime.Now.AddDays(1));

            authDataAccessMoq.GetCredentialsReturnsCredentials(credentials);
            authenticatorMoq.LogInValidatesFor("password", loggedUser);
            authenticatorMoq.IsLoggedReturnsTrueFor(loggedUser);

            LogManager logManager = new LogManager(authenticatorMoq.Object, authDataAccessMoq.Object);

            Token token = logManager.LogIn(new LogInformation("user", "password"));

            token.IsValid().ShouldBeTrue();

            authenticatorMoq.Verify(m=>m.LogIn(credentials, "password"),Times.Once());
            authenticatorMoq.Verify(m => m.IsLogged(loggedUser), Times.Once());
            authDataAccessMoq.Verify(m => m.GetCredentials("user"), Times.Once());
            authDataAccessMoq.Verify(m => m.LogUser(loggedUser), Times.Once());
        }
        [Test]
        public void IfAuthenticationFailsShouldReturnInvalidToken()
        {
            Credentials credentials = new Credentials(Guid.NewGuid(), "user", "saltedPassword");
            LoggedUser loggedUser = new LoggedUser(credentials.UserId, Token.Generate(), DateTime.Now, DateTime.Now.AddDays(1));

            Mock<Authenticator> authenticatorMoq = AuthenticatorMockExtensions.GetMock(nowTimeProviderMoq.Object);
            Mock<AuthDataAccess> authDataAccessMoq = AuthDataAccessMockExtensions.GetMock();

            authDataAccessMoq.GetCredentialsReturnsCredentials(credentials);
            authenticatorMoq.LogInValidatesFor("password", loggedUser);
            authenticatorMoq.IsLoggedReturnsTrueFor(loggedUser);

            LogManager logManager = new LogManager(authenticatorMoq.Object, authDataAccessMoq.Object);

            Token token = logManager.LogIn(new LogInformation("user", "1password"));

            token.IsValid().ShouldBeFalse();

            authenticatorMoq.Verify(m => m.LogIn(credentials, "1password"), Times.Once());
            authenticatorMoq.Verify(m => m.IsLogged(null), Times.Once());
            authDataAccessMoq.Verify(m => m.GetCredentials("user"), Times.Once());
            authDataAccessMoq.Verify(m => m.LogUser(loggedUser), Times.Never);
        }

        [Test]
        public void IfUserIsNotFoundShouldReturnInvalidToken()
        {
            Mock<Authenticator> authenticatorMoq = AuthenticatorMockExtensions.GetMock(nowTimeProviderMoq.Object);
            Mock<AuthDataAccess> authDataAccessMoq = AuthDataAccessMockExtensions.GetMock();

            authDataAccessMoq.GetCredentialsReturnsNull();

            LogManager logManager = new LogManager(authenticatorMoq.Object, authDataAccessMoq.Object);

            Token token = logManager.LogIn(new LogInformation("user","password"));

            token.IsValid().ShouldBeFalse();

            authDataAccessMoq.Verify(m => m.GetCredentials("user"), Times.Once());

        }

    }
}