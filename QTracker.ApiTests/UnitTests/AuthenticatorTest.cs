﻿using Moq;
using NUnit.Framework;
using QTracker.ApiTests.Mocks;
using QTracker.BusinessLogic;
using QTracker.Entities;
using Shouldly;
using System;

namespace QTracker.ApiTests.UnitTests
{
    public class AuthenitcatorUnitTests
    {
        private Mock<IDateTimeProvider> nowTimeProviderMoq;
        private DateTime now;

        [OneTimeSetUp]
        public void Setup()
        {
            now = DateTime.Now;
            nowTimeProviderMoq = DateTimeProviderMock.ForDate(now);
        }

        [Test]
        public void LoggedUserHasNotYetExpired()
        {
            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Token token = Token.Generate();

            LoggedUser loggedUser = new LoggedUser(Guid.NewGuid(), token, now, now.AddDays(1));

            authenticator.IsLogged(loggedUser).ShouldBeTrue();
        }

        [Test]
        public void LoggedUserHasExpired()
        {
            DateTime threeDaysAgo = DateTime.Now.AddDays(-3);
            Mock<IDateTimeProvider> timeProviderMoq = DateTimeProviderMock.ForDate(threeDaysAgo);

            Authenticator authenticator = new Authenticator(timeProviderMoq.Object);
            Token token = Token.Generate();

            LoggedUser loggedUser =
                new LoggedUser(Guid.NewGuid(), token, threeDaysAgo.AddDays(-4), threeDaysAgo.AddDays(-1));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();
        }

        [Test]
        public void LoggedUserHasLoggedInTheFuture()
        {
            DateTime threeDaysAgo = DateTime.Now.AddDays(-3);
            Mock<IDateTimeProvider> timeProviderMoq = DateTimeProviderMock.ForDate(threeDaysAgo);

            Authenticator authenticator = new Authenticator(timeProviderMoq.Object);
            Token token = Token.Generate();

            LoggedUser loggedUser =
                new LoggedUser(Guid.NewGuid(), token, threeDaysAgo.AddDays(1), threeDaysAgo.AddDays(2));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();
        }

        [Test]
        public void LoggedUserWithInvalidToken()
        {
            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Token token = Token.InvalidToken;

            LoggedUser loggedUser = new LoggedUser(Guid.NewGuid(), token, now, now.AddDays(1));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();
        }

        [Test]
        public void LoggedUserWithInvalidUser()
        {
            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Token token = Token.Generate();

            LoggedUser loggedUser = new LoggedUser(Guid.Empty, token, now, now.AddDays(1));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();
        }

        [Test]
        public void ShouldGiveCredentialsAndThenAuthorizeThem()
        {
            UserCreationData data = new UserCreationData("user1", "login1", "password1");

            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Credentials credentials = authenticator.GenerateCredentials(data);
            credentials = new Credentials(Guid.NewGuid(), credentials.Username, credentials.SaltedPassword);
            LoggedUser loggedUser = authenticator.LogIn(credentials, data.Password);
            authenticator.IsLogged(loggedUser).ShouldBeTrue();

        }

        [Test]
        public void ShouldGiveCredentialsAndThenFailToAuthorizeThemWithWrongPassword()
        {
            UserCreationData data = new UserCreationData("user1", "login1", "password1");

            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Credentials credentials = authenticator.GenerateCredentials(data);

            LoggedUser loggedUser = authenticator.LogIn(credentials, "otherPassword");

            authenticator.IsLogged(loggedUser).ShouldBeFalse();

        }

        [Test]
        public void ShouldGiveCredentialsAuthorizeThemAndLogOutTheUserIfEnoughTimeHasPassed()
        {

            UserCreationData data = new UserCreationData("user1", "login1", "password1");

            Authenticator authenticator = new Authenticator(nowTimeProviderMoq.Object);
            Credentials credentials = authenticator.GenerateCredentials(data);
            credentials = new Credentials(Guid.NewGuid(), credentials.Username, credentials.SaltedPassword);
            LoggedUser loggedUser = authenticator.LogIn(credentials, data.Password);
            authenticator.IsLogged(loggedUser).ShouldBeTrue();

            nowTimeProviderMoq.Setup(m => m.GetNow()).Returns(now.AddDays(1));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();

            nowTimeProviderMoq.Setup(m => m.GetNow()).Returns(now.AddDays(-1));

            authenticator.IsLogged(loggedUser).ShouldBeFalse();

        }

    }
}
