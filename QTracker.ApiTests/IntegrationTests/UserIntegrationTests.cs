﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.IntegrationTests
{
 
    public class UserIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;
        private Token token;
        private UserCreationData userData = new UserCreationData("John Smith", "jsmith", "P4ssw0rd!");
        private User user;
        private Customer customer;

        [OneTimeSetUp]
        public void Setup()
        { 
             testServer = new TestServer();
             operations = new OperationBuilder(testServer);
             token = operations.LogValid();
             user = operations.AddUser(new UserCreationData("aNewUser", "withLogin", "andPassword"), token, out _);
             customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
        }
        
        [Test, Order(1)]
        public void AddANewUserAndGetItById()
        {
            User createdUser = operations.AddUser(userData, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);

            createdUser.Name.ShouldBe(userData.Name);
            
            User savedUser = operations.GetUserById(createdUser.UserId, token, out _);

            savedUser.UserId.ShouldBe(createdUser.UserId);
            savedUser.Name.ShouldBe(createdUser.Name);
            
        }

        [Test]
        public void AddANewUserAndGetItBySameUserByUserId()
        {
            UserCreationData anotherUserData = new UserCreationData("JS1", "js1", "P4ssw0rd!");
            User newUser = operations.AddUser(anotherUserData, token, out _);
            Token newToken = operations.Log(new LogInformation(anotherUserData.Login, anotherUserData.Password), out _);
            
            User savedUser = operations.GetUserById(newUser.UserId, newToken, out _);

            savedUser.UserId.ShouldBe(newUser.UserId);
            savedUser.Name.ShouldBe(newUser.Name);

        }

        [Test]
        public void AddANewUserAndGetItAnotherUserByUserId()
        {
            UserCreationData anotherUserData = new UserCreationData("JS2", "js2", "P4ssw0rd!");
            User newUser = operations.AddUser(anotherUserData, token, out _);
            User anotherUser = operations.AddUser(new UserCreationData("newUser1", "newUser1", "newUser1"), token, out _);
            Token anotherToken = operations.Log(new LogInformation("newUser1", "newUser1"), out _);
            
            User savedUser = operations.GetUserById(newUser.UserId, anotherToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test]
        public void AddANewUserAndGetItBySameUserByLogin()
        {
            UserCreationData anotherUserData = new UserCreationData("JS3", "js3", "P4ssw0rd!");
            User newUser = operations.AddUser(anotherUserData, token, out _);
            Token newToken = operations.Log(new LogInformation(anotherUserData.Login, anotherUserData.Password), out _);

            User savedUser = operations.GetUserByLogin(anotherUserData.Login, newToken, out _);

            savedUser.UserId.ShouldBe(newUser.UserId);
            savedUser.Name.ShouldBe(newUser.Name);

        }

        [Test]
        public void AddANewUserAndGetItAnotherUserByLogin()
        {
            UserCreationData anotherUserData = new UserCreationData("JS4", "js4", "P4ssw0rd!");
            User newUser = operations.AddUser(anotherUserData, token, out _);
            User anotherUser = operations.AddUser(new UserCreationData("newUser2", "newUser2", "newUser2"), token, out _);
            Token anotherToken = operations.Log(new LogInformation("newUser2", "newUser2"), out _);

            User savedUser = operations.GetUserByLogin(anotherUserData.Login, anotherToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test, Order(1)]
        public void AddANewUserAndGetItByLogin()
        {
            UserCreationData newUserCreationData = new UserCreationData("name","login","password");
            User createdUser = operations.AddUser(newUserCreationData, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);

            createdUser.Name.ShouldBe(newUserCreationData.Name);

            User savedUser = operations.GetUserByLogin(newUserCreationData.Login, token, out _);

            savedUser.UserId.ShouldBe(createdUser.UserId);
            savedUser.Name.ShouldBe(createdUser.Name);

        }


        [Test, Order(2)]
        public void TryToAddAnAlreadyAddedUserAndFail()
        {
            
            operations.AddUser(userData, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Conflict);

        }

        [Test, Order(3)]
        public void TryToAddAUserWithoutNameAndFail()
        {
            UserCreationData userCreationData = new UserCreationData("","no name", "password");

            operations.AddUser(userCreationData, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddAUserWithoutPasswordAndFail()
        {
            UserCreationData userCreationData = new UserCreationData("name", "no password", "");

            operations.AddUser(userCreationData, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddAUserWithoutLoginAndFail()
        {
            UserCreationData userCreationData = new UserCreationData("no login", "", "password");

            operations.AddUser(userCreationData, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test]
        public void SendInvalidTokenAndFailToWork()
        {
            operations.GetUserById(Guid.NewGuid(), CredentialBuilder.RandomToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }

        [Test]
        public void GetCustomersFromUserBetweenDatesAndShouldFilter()
        {
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            Customer customer2 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);

            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1),token, out _);
            Project project2 = operations.AddProject(ProjectBuilder.RandomProject(customer2), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1,"T1"), token, out _);
            Entities.Task task2 = operations.AddTask(new Entities.Task(project2, "T1"), token, out _);

            operations.AddTimeTrack(new TimeTrack(task1, user, DateTime.Now.AddDays(-2), DateTime.Now),token, out _);
            operations.AddTimeTrack(new TimeTrack(task1, user, DateTime.Now.AddDays(-0.5)), token, out _);
            operations.AddTimeTrack(new TimeTrack(task2, user, DateTime.Now.AddDays(-1.5)), token, out _);

            List<Customer> customers =
                operations.GetCustomersByDate(user.UserId, DateTime.Now.AddDays(-1), DateTime.Now, token, out _);

            customers.Count.ShouldBe(1);

            Customer foundCustomer = customers.Single();

            foundCustomer.Projects.Count.ShouldBe(1);

            Project foundProject = foundCustomer.Projects.Single();

            foundProject.Tasks.Count.ShouldBe(1);

            Entities.Task foundTask = foundProject.Tasks.Single();

            foundTask.TimeTracks.Count.ShouldBe(1);
        }

        [Test]
        [Order(1)]
        public void AddMantainedCustomerToUser()
        {
            User mantainer = operations.GetUserById(user.UserId, token, out _);

            mantainer.Mantains.ShouldBeEmpty();
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out HttpStatusCode statusCode);
            
            statusCode.ShouldBe(HttpStatusCode.Created);

            mantainer = operations.GetUserById(user.UserId, token, out _);

            mantainer.Mantains.ShouldHaveSingleItem();

            CustomerUserAccess customerUserAccess = mantainer.Mantains.Single();

            customerUserAccess.CustomerId.ShouldBe(customer.CustomerId);
            customerUserAccess.UserId.ShouldBe(user.UserId);
            customerUserAccess.Access.ShouldBe(accessInformation.Access);
        }

        [Test]
        [Order(2)]
        public void ModifyCustomerAccessToUser()
        {
            User mantainer = operations.GetUserById(user.UserId, token, out _);

            mantainer.Mantains.ShouldHaveSingleItem();

            CustomerUserAccess customerUserAccess = mantainer.Mantains.Single();

            customerUserAccess.CustomerId.ShouldBe(customer.CustomerId);
            customerUserAccess.UserId.ShouldBe(user.UserId);
            customerUserAccess.Access.ShouldBe(CustomerAccess.Mantainer);

            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);
            operations.ChangeAccessToCustomer(user.UserId, accessInformation, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.OK);

            mantainer = operations.GetUserById(user.UserId, token, out _);

            mantainer.Mantains.ShouldHaveSingleItem();

            customerUserAccess = mantainer.Mantains.Single();

            customerUserAccess.CustomerId.ShouldBe(customer.CustomerId);
            customerUserAccess.UserId.ShouldBe(user.UserId);
            customerUserAccess.Access.ShouldBe(accessInformation.Access);
        }

        [Test]
        [Order(2)]
        public void AddAlreadyExistentMantainedCustomerToUser()
        {
            
            User mantainer = operations.GetUserById(user.UserId, token, out _);

            mantainer.Mantains.ShouldHaveSingleItem();
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Conflict);

            mantainer.Mantains.ShouldHaveSingleItem();

        }

        [Test]
        public void AddMantainerToInexistentUser()
        {
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(Guid.NewGuid(), accessInformation, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        [Test]
        public void AddMantainerToInexistentCustomer()
        {
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(Guid.NewGuid(), CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        [Test]
        public void AddNoAccessToUser()
        {

            UserCreationData newUserCreationData = new UserCreationData("name123", "login123", "password123");
            User createdUser = operations.AddUser(newUserCreationData, token, out _);

            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.None);
            operations.AddAccessToCustomer(createdUser.UserId, accessInformation, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        [Test]
        public void RevokeAccessToUser()
        {

            UserCreationData newUserCreationData = new UserCreationData("name1234", "login1234", "password1234");
            User createdUser = operations.AddUser(newUserCreationData, token, out _);

            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(createdUser.UserId, accessInformation, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);

            User mantainer = operations.GetUserById(createdUser.UserId, token, out _);
            mantainer.Mantains.ShouldHaveSingleItem();
            mantainer.Mantains.Single().Access.ShouldBe(CustomerAccess.Mantainer);

            accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.None);
            operations.ChangeAccessToCustomer(createdUser.UserId, accessInformation, token, out statusCode);
            statusCode.ShouldBe(HttpStatusCode.OK);

            mantainer = operations.GetUserById(createdUser.UserId, token, out _);
            mantainer.Mantains.ShouldHaveSingleItem();
            mantainer.Mantains.Single().Access.ShouldBe(CustomerAccess.None);
        }

    }

}

