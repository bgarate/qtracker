using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.IntegrationTests
{
    public class LogInIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;

        [SetUp]
        public void Setup()
        {
            testServer = new TestServer();
            operations = new OperationBuilder(testServer);
        }

        [Test]
        public void SendCredentialsAndLogInSuccessfully()
        {
            Token token = operations.Log(CredentialBuilder.ValidCredentials, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.OK);
            token.IsValid().ShouldBeTrue("LogIn should have been successfull");
        }

        [Test]
        public void SendCredentialsAndFailToLogIn()
        {
            Token token = operations.Log(CredentialBuilder.InvalidCredentials, out HttpStatusCode statusCode);
            token.IsValid().ShouldBeFalse("LogIn should have been unsuccessfull");
        }

        [Test]
        public void SendCredentialsWithUnsanitizedUsernameAndSucceedLogin()
        {
            LogInformation logInformation = CredentialBuilder.ValidCredentials;
            LogInformation unsanitazedLogInformation =
                new LogInformation($" {logInformation.User.ToUpper()} ", logInformation.Password);
            Token token = operations.Log(unsanitazedLogInformation, out _);

            token.IsValid().ShouldBeTrue("LogIn should have been successfull");
            
        }

    }
}