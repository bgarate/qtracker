﻿using System;
using System.Collections.Generic;
using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;
using Task = QTracker.Entities.Task;

namespace QTracker.ApiTests.IntegrationTests
{
 
    public class TaskIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;
        private Token token;
        private Task task;
        private Project project;
        private Customer customer;

        [OneTimeSetUp]
        public void Setup()
        { 
             testServer = new TestServer();
             operations = new OperationBuilder(testServer);
             token = operations.LogValid();
             customer = operations.AddCustomer(CustomerBuilder.NewCustomer().WithCode("PROJECTTEST")
                 .WithName("Customer for projects").Build(),token,out _);
             project = operations.AddProject(ProjectBuilder.NewProject().Of(customer).WithName("Task test project")
                 .WithDescription("TASK TEST PROJECT DESCRIPTION").Build(),token, out _);
             task = TaskBuilder.NewTask().Of(project).WithDescription("Sample task").Build();
        }
        
        [Test, Order(1)]
        public void AddANewTaskAndGetIt()
        {
            Task createdTask = operations.AddTask(task, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);

            createdTask.ProjectId.ShouldBe(project.ProjectId);
            createdTask.Description.ShouldBe(task.Description);

            Task savedTask = operations.GetTask(createdTask.TaskId, token, out _);

            savedTask.TaskId.ShouldBe(createdTask.TaskId);
            savedTask.ProjectId.ShouldBe(createdTask.ProjectId);
            savedTask.Description.ShouldBe(createdTask.Description);
        }

        [Test, Order(1)]
        public void AddAMultipleTaskWithSameDescriptionAndSucceed()
        {
            Project newProject = operations.AddProject(ProjectBuilder.NewProject().Of(customer).WithName("REPEATED TASKS PROJECT")
                .WithDescription("Description").Build(),token, out _);
            
            TaskBuilder.IBuildable builder = TaskBuilder.NewTask().Of(newProject).WithDescription("RepeatedDescription");

            const int tasksToAdd = 10;

            for (int i = 0; i < tasksToAdd; i++)
            {
                operations.AddTask(builder.Build(), token, out HttpStatusCode statusCode);
                statusCode.ShouldBe(HttpStatusCode.Created);
            }

            List<Task> tasks = operations.GetProject(newProject.ProjectId, token, out _).Tasks;

            tasks.ShouldAllBe(t=>t.Description == builder.Build().Description);

        }

        [Test, Order(3)]
        public void TryToAddATaskWithoutDescriptionAndFail()
        {
            Task task = TaskBuilder.NewTask().Of(project).WithDescription("").Build();

            operations.AddTask(task, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddATaskOfInvalidProjectAndFail()
        {
            Project newProject = ProjectBuilder.NewProject().Of(customer).WithName("NAME").WithDescription("DESCRIPTION").Build();
            newProject.ProjectId = Guid.NewGuid();
            Task newTask = TaskBuilder.NewTask().Of(newProject).WithDescription("NewTask").Build();

            operations.AddTask(newTask, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test]
        public void SendInvalidTokenAndFailToWork()
        {
            Task newTask =
                operations.AddTask(TaskBuilder.NewTask().Of(project).WithDescription("Sample task 2").Build(), token,
                    out _);
            operations.GetTask(newTask.TaskId, CredentialBuilder.RandomToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }

        [Test]
        public void AddANewTaskFromAnAccessUser()
        {
            User user = operations.AddUser(new UserCreationData("u1", "u1", "u1"), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out _);
            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Token newToken = operations.Log(new LogInformation("u1", "u1"), out _);
            
            Task task = TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build();
            Task createdTask = operations.AddTask(task, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);
        }

        [Test]
        public void GetTaskFromAnAccessUser()
        {
            User user = operations.AddUser(new UserCreationData("m1", "m1", "m1"), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out _);
            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Token newToken = operations.Log(new LogInformation("m1", "m1"), out _);

            Task task = TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build();
            Task createdTask = operations.AddTask(task, token, out _);

            Task obtainedTask = operations.GetTask(createdTask.TaskId, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.OK);
            obtainedTask.Description.ShouldBe(task.Description);
        }

        [Test]
        public void GetTaskFromANonAccessUser()
        {
            User user = operations.AddUser(new UserCreationData("m2", "m2", "m2"), token, out _);
            
            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Token newToken = operations.Log(new LogInformation("m2", "m2"), out _);

            Task task = TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build();
            Task createdTask = operations.AddTask(task, token, out _);

            Task obtainedTask = operations.GetTask(createdTask.TaskId, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test]
        public void AddANewTaskFromANonAccessUser()
        {
            User user = operations.AddUser(new UserCreationData("u2", "u2", "u2"), token, out _);
            
            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Token newToken = operations.Log(new LogInformation("u2", "u2"), out _);

            Task task = TaskBuilder.NewTask().Of(newProject).WithDescription("T2").Build();
            Task createdTask = operations.AddTask(task, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }
    }
}
