﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.IntegrationTests
{
 
    public class ProjectIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;
        private Token token;
        private Project project;
        private Customer customer;

        [OneTimeSetUp]
        public void Setup()
        { 
             testServer = new TestServer();
             operations = new OperationBuilder(testServer);
             token = operations.LogValid();
             customer = operations.AddCustomer(CustomerBuilder.NewCustomer().WithCode("PROJECTTEST")
                 .WithName("Customer for projects").Build(),token,out _);
             project = ProjectBuilder.NewProject().Of(customer).WithName("Sample project")
                 .WithDescription("SAMPLE DESCRIPTION").Build();
        }
        
        [Test, Order(1)]
        public void AddANewProjectAndGetIt()
        {
            Project createdProject = operations.AddProject(project, token, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);

            createdProject.CustomerId.ShouldBe(customer.CustomerId);
            createdProject.Name.ShouldBe(project.Name);
            createdProject.Description.ShouldBe(project.Description);

            Project savedProject = operations.GetProject(createdProject.ProjectId, token, out _);

            savedProject.ProjectId.ShouldBe(createdProject.ProjectId);
            savedProject.CustomerId.ShouldBe(createdProject.CustomerId);
            savedProject.Name.ShouldBe(createdProject.Name);
            savedProject.Description.ShouldBe(createdProject.Description);
        }

        [Test]
        public void AddANewProjectFromANotMantainerUser()
        {
            User user = operations.AddUser(new UserCreationData("u4", "u4", "u4"), token, out _);
            Token newToken = operations.Log(new LogInformation("u4", "u4"), out _);

            Project createdProject = operations.AddProject(project, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);

        }

        [Test]
        public void GetAProjectFromANotMantainerUser()
        {
            User user = operations.AddUser(new UserCreationData("m2", "m2", "m2"), token, out _);
            Token newToken = operations.Log(new LogInformation("m2", "m2"), out _);

            Project createdProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);

            Project obtainedProject = operations.GetProject(createdProject.ProjectId, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);

        }

        [Test]
        public void GetAProjectFromAMantainerUser()
        {
            User user = operations.AddUser(new UserCreationData("m1", "m1", "m1"), token, out _);
            Token newToken = operations.Log(new LogInformation("m1", "m1"), out _);

            operations.AddAccessToCustomer(user.UserId,
                new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access), token, out _);
            Project createdProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);

            Project obtainedProject = operations.GetProject(createdProject.ProjectId, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.OK);
            obtainedProject.Name.ShouldBe(createdProject.Name);

        }

        [Test]
        public void AddANewProjectFromAMantainerUser()
        {
            User user = operations.AddUser(new UserCreationData("u3", "u3", "u3"), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out  _);
            Project newProject = ProjectBuilder.RandomProject(customer);
            Token newToken = operations.Log(new LogInformation("u3", "u3"), out _);

            Project createdProject = operations.AddProject(newProject, newToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Created);
        }

        [Test]
        public void AddANewProjectFromAnAccessUser()
        {
            User user = operations.AddUser(new UserCreationData("u2", "u2", "u2"), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);
            operations.AddAccessToCustomer(user.UserId, accessInformation, token, out _);

            Token newToken = operations.Log(new LogInformation("u2", "u2"), out _);

            Project createdProject = operations.AddProject(project, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }


        [Test, Order(2)]
        public void TryToAddAnAlreadyAddedProjectAndFail()
        {
            List<Project> projects = operations.GetCustomerById(customer.CustomerId, token, out _).Projects;

            projects.ShouldContain(p => p.Name == project.Name);

            project.Name = project.Name.ToUpper();
            operations.AddProject(project, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Conflict);

            projects = operations.GetCustomerById(customer.CustomerId, token, out _).Projects;

            projects.ShouldNotContain(p1 => projects.Any(p2 => !ReferenceEquals(p1, p2) && p1.Name == p2.Name));

        }


        [Test, Order(2)]
        public void TryToAddAProjectWithSameNameInDifferentsCustomersAndSucceed()
        {
            Customer customer1 =
                operations.AddCustomer(CustomerBuilder.NewCustomer().WithCode("C1").WithName("C1").Build(), token,
                    out _);
            Customer customer2 =
                operations.AddCustomer(CustomerBuilder.NewCustomer().WithCode("C2").WithName("C2").Build(), token,
                    out _);
            Project newProject = ProjectBuilder.NewProject().Of(customer1).WithName("P").WithDescription("Desc")
                .Build();
            
            operations.AddProject(newProject, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Created);

            newProject.CustomerId = customer2.CustomerId;

            operations.AddProject(newProject, token, out statusCode);

            statusCode.ShouldBe(HttpStatusCode.Created);

            List<Project> projects = operations.GetCustomerById(customer1.CustomerId, token, out _).Projects;

            projects.ShouldContain(p=>p.Name == newProject.Name);

            projects = operations.GetCustomerById(customer2.CustomerId, token, out _).Projects;

            projects.ShouldContain(p => p.Name == newProject.Name);

        }

        [Test, Order(3)]
        public void TryToAddAProjectWithoutNameAndFail()
        {
            Project project = ProjectBuilder.NewProject().Of(customer).WithName("").WithDescription("Without name").Build();

            operations.AddProject(project, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddAProjectWithoutDescriptionAndFail()
        {
            Project project = ProjectBuilder.NewProject().Of(customer).WithName("Without name").WithDescription("").Build();

            operations.AddProject(project, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddAProjectOfInvalidCustomerAndFail()
        {
            Customer newCustomer = CustomerBuilder.NewCustomer().WithCode("CODE1").WithName("NAME1").Build();
            newCustomer.CustomerId = Guid.NewGuid();
            Project newProject = ProjectBuilder.NewProject().Of(newCustomer).WithName("NAME2").WithDescription("DESCRIPTION2").Build();

            operations.AddProject(newProject, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test]
        public void SendInvalidTokenAndFailToWork()
        {
            operations.GetProject(Guid.NewGuid(), CredentialBuilder.RandomToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
    }
}
