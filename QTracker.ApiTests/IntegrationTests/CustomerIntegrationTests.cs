﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.IntegrationTests
{
 
    public class CustomerIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;
        private Token token;
        private Customer customer;

        [OneTimeSetUp]
        public void Setup()
        { 
             testServer = new TestServer();
             operations = new OperationBuilder(testServer);
             token = operations.LogValid();
             customer = CustomerBuilder.NewCustomer().WithCode("CUST1").WithName("Customer 1").Build();
        }

        [Test,Order(0)]
        public void GetAllCustomersAndGetSomeResults()
        {
            List<Customer> customers = operations.GetCustomers(token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.OK);
            customers.ShouldNotBeEmpty();
        }

        [Test, Order(0)]
        public void GetAllCustomersAndGetNoResultsForUserWithoutAccess()
        {
            User user = operations.AddUser(new UserCreationData("username1", "username1", "password"), token, out _);
            Token tokenForUser = operations.Log(new LogInformation("username1", "password"), out _);
            List<Customer> customers = operations.GetCustomers(tokenForUser, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.OK);
            customers.ShouldBeEmpty();
        }

        [Test, Order(0)]
        public void GetAllCustomersAndGetResultsForUserWithAccess()
        {
            User user = operations.AddUser(new UserCreationData("username1", "username1", "password"), token, out _);
            Customer newCustomer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            Token tokenForUser = operations.Log(new LogInformation("username1", "password"), out _);

            operations.AddAccessToCustomer(user.UserId,
                new CustomerAccessInformation(newCustomer.CustomerId, CustomerAccess.Access), token, out _);

            List<Customer> customers = operations.GetCustomers(tokenForUser, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.OK);
            customers.ShouldHaveSingleItem();
            Customer customerObtained = customers.Single();

            customerObtained.CustomerId.ShouldBe(newCustomer.CustomerId);
        }


        [Test, Order(1)]
        public void GetAllCustomersWithInvalidToken()
        {
            List<Customer> customers = operations.GetCustomers(Token.InvalidToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
            customers.ShouldBeNull();

        }

        [Test, Order(2)]
        public void AddANewCustomerAndGetItByQueringAllCustomers()
        {
            List<Customer> customers = operations.GetCustomers(token, out _);

            customers.ShouldNotContain(c => c.Code == customer.Code);

            Customer createdCustomer = operations.AddCustomer(customer,token, out _);

            createdCustomer.Code.ShouldBe(customer.Code);
            createdCustomer.Name.ShouldBe(customer.Name);

            customers = operations.GetCustomers(token, out _);

            customers.ShouldContain(c => c.Code == customer.Code);

            Customer savedCustomer = customers.Single(c => c.Code == customer.Code);

            savedCustomer.Name.ShouldBe(customer.Name);
            
        }

        [Test, Order(2)]
        public void AddANewCustomerAndGetItByCode()
        {
            List<Customer> customers = operations.GetCustomers(token, out _);

            const string customerCode = "TEST-CUSTOMER";
            customers.ShouldNotContain(c => c.Code == customerCode);

            Customer newCustomer = CustomerBuilder.NewCustomer().WithCode(customerCode).WithName("Name").Build();
            Customer createdCustomer = operations.AddCustomer(newCustomer, token, out _);

            createdCustomer.Code.ShouldBe(newCustomer.Code);
            createdCustomer.Name.ShouldBe(newCustomer.Name);

            Customer savedCustomer = operations.GetCustomerByCode(customerCode.ToLower(),token, out _);

            savedCustomer.CustomerId.ShouldBe(createdCustomer.CustomerId);
            savedCustomer.Code.ShouldBe(createdCustomer.Code);
            savedCustomer.Name.ShouldBe(createdCustomer.Name);

        }

        [Test, Order(2)]
        public void AddANewCustomerAndGetItById()
        {
            List<Customer> customers = operations.GetCustomers(token, out _);
            const string customerCode = "TEST-CUSTOMER-2";
            customers.ShouldNotContain(c => c.Code == customerCode);

            Customer newCustomer = CustomerBuilder.NewCustomer().WithCode(customerCode).WithName("Another Name").Build();
            Customer createdCustomer = operations.AddCustomer(newCustomer, token, out _);

            createdCustomer.Code.ShouldBe(newCustomer.Code);
            createdCustomer.Name.ShouldBe(newCustomer.Name);

            Customer savedCustomer = operations.GetCustomerById(createdCustomer.CustomerId, token, out _);

            savedCustomer.CustomerId.ShouldBe(createdCustomer.CustomerId);
            savedCustomer.Code.ShouldBe(createdCustomer.Code);
            savedCustomer.Name.ShouldBe(createdCustomer.Name);

        }


        [Test, Order(3)]
        public void TryToAddAnAlreadyAddedCustomerAndFail()
        {
            List<Customer> customers = operations.GetCustomers(token, out _);

            customers.ShouldContain(c => c.Code == customer.Code);

            operations.AddCustomer(customer, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Conflict);

            customers = operations.GetCustomers(token, out _);

            customers.ShouldNotContain(c1 => customers.Any(c2 => !ReferenceEquals(c1, c2) && c1.Code == c2.Code));



        }

        [Test, Order(3)]
        public void TryToAddACustomerWithoutNameAndFail()
        {
            Customer customer = CustomerBuilder.NewCustomer().WithCode("Without name").WithName("").Build();

            operations.AddCustomer(customer, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }

        [Test, Order(3)]
        public void TryToAddACustomerWithoutCodeAndFail()
        {
            Customer customer = CustomerBuilder.NewCustomer().WithCode("").WithName("Without code").Build();

            operations.AddCustomer(customer, token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.BadRequest);

        }


        [Test]
        public void SendInvalidTokenAndFailToWork()
        {
            operations.GetCustomers(CredentialBuilder.RandomToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
    }
}
