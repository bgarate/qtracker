﻿using System;
using System.Net;
using NUnit.Framework;
using QTracker.ApiTests.Helpers;
using QTracker.Entities;
using Shouldly;

namespace QTracker.ApiTests.IntegrationTests
{
 
    public class TimeTrackIntegrationTests
    {
        private TestServer testServer;
        private OperationBuilder operations;
        private Token token;
        private User user;

        [OneTimeSetUp]
        public void Setup()
        { 
             testServer = new TestServer();
             operations = new OperationBuilder(testServer);
             token = operations.LogValid();
             user = operations.AddUser(new UserCreationData("aNewUser", "withLogin", "andPassword"), token, out _);
        }

        [Test]
        public void SendInvalidTokenAndFailToWork()
        {
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);

            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1, "T2"), token, out _);

            operations.AddTimeTrack(new TimeTrack(task1, user, DateTime.Now, DateTime.Now.AddDays(1)),
                CredentialBuilder.RandomToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
        [Test]
        public void CreateANewTimeTrackAsAdminAndGetIt()
        {
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);

            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1, "T1"), token, out _);

            TimeTrack timeTrack = operations.AddTimeTrack(new TimeTrack(task1, user, new DateTime(2019, 10, 1), new DateTime(2019, 10, 2)),
                token, out _);

            TimeTrack savedTimeTrack = operations.GetTimeTrack(timeTrack.TimeTrackId, token, out _);

            savedTimeTrack.TimeTrackId.ShouldBe(timeTrack.TimeTrackId);
            savedTimeTrack.Task.TaskId.ShouldBe(timeTrack.TaskId);
            savedTimeTrack.Start.ShouldBe(timeTrack.Start);
            savedTimeTrack.End.ShouldBe(timeTrack.End);
            savedTimeTrack.UserId.ShouldBe(timeTrack.UserId);
            savedTimeTrack.Task.Project.ProjectId.ShouldBe(project1.ProjectId);
            savedTimeTrack.Task.Project.Customer.CustomerId.ShouldBe(customer1.CustomerId);

        }
        [Test]
        public void CreateANewTimeTrackForAnotherUserAsAdminAndGetIt()
        {
            User newUser = operations.AddUser(new UserCreationData("user4", "user4", "user4"), token, out _);
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer1.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);
            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1, "T1"), token, out _);

            Token newToken = operations.Log(new LogInformation("user4", "user4"), out _);

            TimeTrack timeTrack = operations.AddTimeTrack(new TimeTrack(task1, newUser, new DateTime(2019, 10, 1), new DateTime(2019, 10, 2)),
                token, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Created);

            TimeTrack savedTimeTrack = operations.GetTimeTrack(timeTrack.TimeTrackId, newToken, out _);

            savedTimeTrack.TimeTrackId.ShouldBe(timeTrack.TimeTrackId);
            savedTimeTrack.Task.TaskId.ShouldBe(timeTrack.TaskId);
            savedTimeTrack.Start.ShouldBe(timeTrack.Start);
            savedTimeTrack.End.ShouldBe(timeTrack.End);
            savedTimeTrack.UserId.ShouldBe(timeTrack.UserId);
            savedTimeTrack.Task.Project.ProjectId.ShouldBe(project1.ProjectId);
            savedTimeTrack.Task.Project.Customer.CustomerId.ShouldBe(customer1.CustomerId);
        }

        [Test]
        public void CreateANewTimeTrackAndGetIt()
        {
            User newUser = operations.AddUser(new UserCreationData("user1", "user1", "user1"), token, out _);
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer1.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);
            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1, "T1"), token, out _);
            
            Token newToken = operations.Log(new LogInformation("user1", "user1"), out _);

            TimeTrack timeTrack = operations.AddTimeTrack(new TimeTrack(task1, newUser, new DateTime(2019, 10, 1), new DateTime(2019, 10, 2)),
                newToken, out _);

            TimeTrack savedTimeTrack = operations.GetTimeTrack(timeTrack.TimeTrackId, newToken, out _);

            savedTimeTrack.TimeTrackId.ShouldBe(timeTrack.TimeTrackId);
            savedTimeTrack.Task.TaskId.ShouldBe(timeTrack.TaskId);
            savedTimeTrack.Start.ShouldBe(timeTrack.Start);
            savedTimeTrack.End.ShouldBe(timeTrack.End);
            savedTimeTrack.UserId.ShouldBe(timeTrack.UserId);
            savedTimeTrack.Task.Project.ProjectId.ShouldBe(project1.ProjectId);
            savedTimeTrack.Task.Project.Customer.CustomerId.ShouldBe(customer1.CustomerId);

        }
        [Test]
        public void CreateANewTimeTrackForAnotherUser()
        {
            User newUser = operations.AddUser(new UserCreationData("user2", "user2", "user2"), token, out _);
            User anotherUser = operations.AddUser(new UserCreationData("user3", "user3", "user3"), token, out _);
            Customer customer1 = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer1.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);
            operations.AddAccessToCustomer(anotherUser.UserId, accessInformation, token, out _);

            Project project1 = operations.AddProject(ProjectBuilder.RandomProject(customer1), token, out _);

            Entities.Task task1 = operations.AddTask(new Entities.Task(project1, "T1"), token, out _);

            Token newToken = operations.Log(new LogInformation("user2", "user2"), out _);
            Token anotherToken = operations.Log(new LogInformation("user3", "user3"), out _);

            TimeTrack timeTrack = operations.AddTimeTrack(new TimeTrack(task1, newUser, new DateTime(2019, 10, 1), new DateTime(2019, 10, 2)),
                newToken, out _);

            TimeTrack savedTimeTrack = operations.GetTimeTrack(timeTrack.TimeTrackId, anotherToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test]
        public void AddANewTimeTrackFromAnAccessUser()
        {
            User newUser = operations.AddUser(new UserCreationData("u1", "u1", "u1"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(),token, out _);
            
            Token newToken = operations.Log(new LogInformation("u1", "u1"), out _);

            TimeTrack timeTrack = new TimeTrack(task,newUser, DateTime.Now, DateTime.Today.AddDays(1));
            operations.AddTimeTrack(timeTrack, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Created);
        }

        [Test]
        public void GetTimeTrackFromAnAccessUser()
        {
            User newUser = operations.AddUser(new UserCreationData("m1", "m1", "m1"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("m1", "m1"), out _);

            TimeTrack timeTrack = new TimeTrack(task, newUser, DateTime.Now, DateTime.Today.AddDays(1));
            TimeTrack createdTimeTrack = operations.AddTimeTrack(timeTrack, token, out _);

            TimeTrack obtainedTimeTrack =
                operations.GetTimeTrack(createdTimeTrack.TimeTrackId, newToken, out HttpStatusCode statusCode);
            
            obtainedTimeTrack.TimeTrackId.ShouldBe(createdTimeTrack.TimeTrackId);
            statusCode.ShouldBe(HttpStatusCode.OK);
        }

        [Test]
        public void GetTimeTrackFromAnotherUserAsAdmin()
        {
            User newUser = operations.AddUser(new UserCreationData("m7", "m7", "m7"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("m7", "m7"), out _);

            TimeTrack timeTrack = new TimeTrack(task, newUser, DateTime.Now, DateTime.Today.AddDays(1));
            TimeTrack createdTimeTrack = operations.AddTimeTrack(timeTrack, token, out _);

            TimeTrack obtainedTimeTrack =
                operations.GetTimeTrack(createdTimeTrack.TimeTrackId, token, out HttpStatusCode statusCode);

            obtainedTimeTrack.TimeTrackId.ShouldBe(createdTimeTrack.TimeTrackId);
            statusCode.ShouldBe(HttpStatusCode.OK);
        }

        [Test]
        public void GetTimeTrackFromANonAccessUser()
        {
            User newUser = operations.AddUser(new UserCreationData("m2", "m2", "m2"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("m2", "m2"), out _);

            TimeTrack timeTrack = new TimeTrack(task, newUser, DateTime.Now, DateTime.Today.AddDays(1));
            TimeTrack createdTimeTrack = operations.AddTimeTrack(timeTrack, token, out _);

            TimeTrack obtainedTimeTrack =
                operations.GetTimeTrack(createdTimeTrack.TimeTrackId, newToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test]
        public void GetTimeTrackFromAnotherUserAsMantainer()
        {
            User newUser = operations.AddUser(new UserCreationData("m3", "m3", "m3"), token, out _);
            User anotherUser = operations.AddUser(new UserCreationData("m4", "m4", "m4"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Mantainer);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);
            operations.AddAccessToCustomer(anotherUser.UserId, accessInformation, token, out _);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("m3", "m3"), out _);
            Token anotherToken = operations.Log(new LogInformation("m4", "m4"), out _);

            TimeTrack timeTrack = new TimeTrack(task, anotherUser, DateTime.Now, DateTime.Today.AddDays(1));
            TimeTrack createdTimeTrack = operations.AddTimeTrack(timeTrack, anotherToken, out _);

            TimeTrack obtainedTimeTrack =
                operations.GetTimeTrack(createdTimeTrack.TimeTrackId, newToken, out HttpStatusCode statusCode);

            obtainedTimeTrack.TimeTrackId.ShouldBe(createdTimeTrack.TimeTrackId);
            statusCode.ShouldBe(HttpStatusCode.OK);
        }

        [Test]
        public void GetTimeTrackFromAnotherUserAsAccess()
        {
            User newUser = operations.AddUser(new UserCreationData("m5", "m5", "m5"), token, out _);
            User anotherUser = operations.AddUser(new UserCreationData("m6", "m6", "m6"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            CustomerAccessInformation accessInformation = new CustomerAccessInformation(customer.CustomerId, CustomerAccess.Access);

            operations.AddAccessToCustomer(newUser.UserId, accessInformation, token, out _);
            operations.AddAccessToCustomer(anotherUser.UserId, accessInformation, token, out _);

            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T1").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("m5", "m5"), out _);
            Token anotherToken = operations.Log(new LogInformation("m6", "m6"), out _);

            TimeTrack timeTrack = new TimeTrack(task, anotherUser, DateTime.Now, DateTime.Today.AddDays(1));
            TimeTrack createdTimeTrack = operations.AddTimeTrack(timeTrack, anotherToken, out _);

            TimeTrack obtainedTimeTrack =
                operations.GetTimeTrack(createdTimeTrack.TimeTrackId, newToken, out HttpStatusCode statusCode);

            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

        [Test]
        public void AddANewTimeTrackFromANonAccessUser()
        {
            User newUser = operations.AddUser(new UserCreationData("u2", "u2", "u2"), token, out _);
            Customer customer = operations.AddCustomer(CustomerBuilder.RandomCustomer(), token, out _);
            
            Project newProject = operations.AddProject(ProjectBuilder.RandomProject(customer), token, out _);
            Task task = operations.AddTask(TaskBuilder.NewTask().Of(newProject).WithDescription("T21").Build(), token, out _);

            Token newToken = operations.Log(new LogInformation("u2", "u2"), out _);

            TimeTrack timeTrack = new TimeTrack(task, newUser, DateTime.Now, DateTime.Today.AddDays(1));
            operations.AddTimeTrack(timeTrack, newToken, out HttpStatusCode statusCode);
            statusCode.ShouldBe(HttpStatusCode.Forbidden);
        }

    }
}

