using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using QTracker.Entities;
using QTracker.Services;

namespace QTracker.ApiTests.Helpers
{
    public class TestServer {

        public string Path { get; } = "http://localhost:44341/api/";
        private WebApplicationFactory<Startup> Factory { get; }
        private HttpClient Client { get;  }

        public TestServer()
        {
            Factory = new WebApplicationFactory<Startup>();
            Client = Factory.CreateClient();
        }

        public Output Post<Input, Output>(string operation, Input body, out HttpStatusCode statusCode, Token token = null)
            => Operation<Input, Output>(HttpMethod.Post, operation, body, out statusCode, token);

        public Output Patch<Input, Output>(string operation, Input body, out HttpStatusCode statusCode, Token token = null)
            => Operation<Input, Output>(HttpMethod.Patch, operation, body, out statusCode, token);

        public Output Get<Input, Output>(string operation, Input body, out HttpStatusCode statusCode, Token token = null)
            => Operation<Input, Output>(HttpMethod.Get, operation, body, out statusCode, token);


        private Output Operation<Input, Output>(HttpMethod method, string operation, Input body, out HttpStatusCode statusCode, Token token = null)
        {

            DefaultContractResolver contractResolver = new CamelCasePropertyNamesContractResolver();

            JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
            {
                SerializerSettings =
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    ContractResolver =  contractResolver
                }
            };

            HttpRequestMessage request = new HttpRequestMessage(method, Path + operation)
            {
                Content = new StringContent(JsonConvert.SerializeObject(body, jsonFormatter.SerializerSettings))
            };

            if(token != null)
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.Value);

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = Client.SendAsync(request).Result;

            string responseString = response.Content.ReadAsStringAsync().Result;

            statusCode = response.StatusCode;

            if(statusCode == HttpStatusCode.InternalServerError)
                Console.WriteLine(responseString);

            Output result;

            try
            {
                result = JsonConvert.DeserializeObject<Output>(responseString);
            }
            catch (JsonReaderException)
            {
                result = default;
            }
            
            return result;

        }

    }
}