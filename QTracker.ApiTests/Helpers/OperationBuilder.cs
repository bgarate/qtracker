using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using QTracker.Entities;
using Task = System.Threading.Tasks.Task;

namespace QTracker.ApiTests.Helpers
{
    public class OperationBuilder
    {
        private readonly TestServer testServer;
        public OperationBuilder(TestServer testServer)
        {
            this.testServer = testServer;
        }

        public Token Log(LogInformation logInformation, out HttpStatusCode statusCode) =>
            testServer.Post<LogInformation, Token>("log", logInformation, out statusCode);

        public Token LogValid() => Log(CredentialBuilder.ValidCredentials, out _);

        public List<Customer> GetCustomers(Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, List<Customer>>("customer",null, out statusCode,token);

        public Customer GetCustomerByCode(string code, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, Customer>($"customer/{code}", null, out statusCode, token);

        public Customer GetCustomerById(Guid customerId, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, Customer>($"customer/{customerId}", null, out statusCode, token);

        public Customer AddCustomer(Customer customer, Token token, out HttpStatusCode statusCode) =>
            testServer.Post<Customer, Customer>("customer", customer, out statusCode, token);

        public User GetUserByLogin(string login, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, User>($"user/{login}", null, out statusCode, token);

        public User GetUserById(Guid userId, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, User>($"user/{userId}", null, out statusCode, token);


        public Project AddProject(Project project, Token token, out HttpStatusCode statusCode) =>
            testServer.Post<Project, Project>("project", project, out statusCode, token);
        public User AddUser(UserCreationData userCreationData, Token token, out HttpStatusCode statusCode) =>
            testServer.Post<UserCreationData, User>("user", userCreationData, out statusCode, token);

        public Project GetProject(Guid projectId, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, Project>($"project/{projectId}", null, out statusCode, token);
        
        public QTracker.Entities.Task AddTask(QTracker.Entities.Task task, Token token, out HttpStatusCode statusCode) =>
            testServer.Post<QTracker.Entities.Task, QTracker.Entities.Task>("task", task, out statusCode, token);

        public QTracker.Entities.Task GetTask(Guid taskId, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, QTracker.Entities.Task>($"task/{taskId}", null, out statusCode, token);

        public List<Customer> GetCustomersByDate(Guid userId, DateTime start, DateTime end, Token token,
            out HttpStatusCode statusCode) =>
            testServer.Get<object, List<Customer>>($"user/{userId}/tracking?start={start:u}&end={end:u}", null,
                out statusCode, token);

        public TimeTrack AddTimeTrack(QTracker.Entities.TimeTrack timeTrack, Token token, out HttpStatusCode statusCode) =>
            testServer.Post<TimeTrack, TimeTrack>("timeTrack", timeTrack, out statusCode, token);

        public TimeTrack GetTimeTrack(Guid timeTrackId, Token token, out HttpStatusCode statusCode) =>
            testServer.Get<object, TimeTrack>($"timeTrack/{timeTrackId}", null, out statusCode, token);

        public void AddAccessToCustomer(Guid userId, CustomerAccessInformation accessInformation, Token token,
            out HttpStatusCode statusCode) =>
            testServer.Post<CustomerAccessInformation, object>($"user/{userId}/accesses", accessInformation,
                out statusCode, token);

        public void ChangeAccessToCustomer(Guid userId, CustomerAccessInformation accessInformation, Token token,
            out HttpStatusCode statusCode) =>
            testServer.Patch<CustomerAccessInformation, object>($"user/{userId}/accesses", accessInformation,
                out statusCode, token);

    }
}