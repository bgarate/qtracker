﻿using System;
using QTracker.Entities;

namespace QTracker.ApiTests.Helpers
{
    public class ProjectBuilder : ProjectBuilder.IDescriptable, ProjectBuilder.INameable, ProjectBuilder.IBuildable, ProjectBuilder.IFrom
    {
        private string Name;
        private string Description;
        private Customer Customer;
        private static Random Rnd = new Random();

        private ProjectBuilder()
        {

        }

        public static Project RandomProject(Customer customer)
        {
            return new Project(customer, $"P-{Rnd.Next()}", "RANDOM PROJECT");
        }
        private ProjectBuilder(Customer customer, string name, string description)
        {
            Customer = customer;
            Name = name;
            Description = description;
        }

        public static IFrom NewProject()
        {
            return new ProjectBuilder();
        }

        public IDescriptable WithName(string name)
        {
            Name = name;
            return new ProjectBuilder(Customer, Name, Description);
        }

        public INameable Of(Customer customer)
        {
            Customer = customer;
            return new ProjectBuilder(Customer, Name, Description);
        }

        public IBuildable WithDescription(string description)
        {
            Description = description;
            return new ProjectBuilder(Customer, Name, Description);
        }

        public Project Build()
        {
            return new Project(Customer, Name, Description);
        }

        public interface INameable
        {
            IDescriptable WithName(string name);
        }

        public interface IDescriptable
        {
            IBuildable WithDescription(string description);
        }

        public interface IFrom
        {
            INameable Of(Customer customer);
        }

        public interface IBuildable
        {
            Project Build();
        }
    }
}