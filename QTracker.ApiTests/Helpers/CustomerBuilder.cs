﻿using System;
using QTracker.Entities;

namespace QTracker.ApiTests.Helpers
{
    public class CustomerBuilder : CustomerBuilder.ICodeable, CustomerBuilder.INameable, CustomerBuilder.IBuildable
    {
        private string Name;
        private string Code;
        
        private static Random Rnd = new Random();

        private CustomerBuilder()
        {

        }

        private CustomerBuilder(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public static Customer RandomCustomer()
        {
            return new Customer($"C-{Rnd.Next()}", "RANDOM CUSTOMER");
        }

        public static ICodeable NewCustomer()
        {
            return new CustomerBuilder();
        }

        public IBuildable WithName(string name)
        {
            Name = name;
            return new CustomerBuilder(Name, Code);
        }

        public INameable WithCode(string code)
        {
            Code = code;
            return new CustomerBuilder(Name, Code);
        }

        public Customer Build()
        {
            return new Customer(Code, Name);
        }

        public interface INameable
        {
            IBuildable WithName(string name);
        }

        public interface ICodeable
        {
            INameable WithCode(string code);
        }

        public interface IBuildable
        {
            Customer Build();
        }
    }
}