using System;
using QTracker.Entities;

namespace QTracker.ApiTests.Helpers
{
    public static class CredentialBuilder
    {
        public static LogInformation ValidCredentials => new LogInformation("testUser", "testPassword");
        public static LogInformation InvalidCredentials => new LogInformation("test", "test");

        public static Token RandomToken => new Token(Guid.NewGuid().ToString());
    }
}