﻿using QTracker.Entities;

namespace QTracker.ApiTests.Helpers
{
    public class TaskBuilder : TaskBuilder.IDescriptable, TaskBuilder.IBuildable, TaskBuilder.IFrom
    {
        private string Description;
        private Project Project;

        private TaskBuilder()
        {

        }

        private TaskBuilder(Project project, string description)
        {
            Project = project;
            Description = description;
        }

        public static IFrom NewTask()
        {
            return new TaskBuilder();
        }

        public IDescriptable Of(Project project)
        {
            Project = project;
            return new TaskBuilder(Project, Description);
        }

        public IBuildable WithDescription(string description)
        {
            Description = description;
            return new TaskBuilder(Project, Description);
        }

        public Task Build()
        {
            return new Task(Project, Description);
        }

        public interface IDescriptable
        {
            IBuildable WithDescription(string description);
        }

        public interface IFrom
        {
            IDescriptable Of(Project project);
        }

        public interface IBuildable
        {
            Task Build();
        }
    }
}