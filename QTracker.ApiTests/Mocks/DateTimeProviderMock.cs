﻿using System;
using Moq;
using QTracker.BusinessLogic;

namespace QTracker.ApiTests.Mocks
{
    public static class DateTimeProviderMock
    {
        public static Mock<IDateTimeProvider> ForDate(DateTime dateTime)
        {
            Mock<IDateTimeProvider> nowTimeProviderMoq = new Mock<IDateTimeProvider>();
            nowTimeProviderMoq.Setup(m => m.GetNow()).Returns(dateTime);

            return nowTimeProviderMoq;
        }
    }
}