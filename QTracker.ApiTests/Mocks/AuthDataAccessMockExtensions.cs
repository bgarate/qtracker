﻿using System;
using Microsoft.EntityFrameworkCore;
using Moq;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.ApiTests.Mocks
{
    public static class AuthDataAccessMockExtensions
    {
        private static readonly QTrackerContext dummyContext = new QTrackerContext(new DbContextOptions<QTrackerContext>());
        public static Mock<AuthDataAccess> GetMock()=> new Mock<AuthDataAccess>(dummyContext);
        public static void GetCredentialsReturnsNull(this Mock<AuthDataAccess> mock)
        {
            mock.Setup(m => m.GetCredentials(It.IsNotNull<string>()))
                .Returns<string>(null);
        }

        public static void GetCredentialsReturnsCredentials(this Mock<AuthDataAccess> mock)
        {
            mock.Setup(m => m.GetCredentials(It.IsNotNull<string>()))
                .Returns<string>(u=>new Credentials(Guid.NewGuid(), u, "saltedPassword"));
        }

        public static void GetCredentialsReturnsCredentials(this Mock<AuthDataAccess> mock, Credentials credentials)
        {
            mock.Setup(m => m.GetCredentials(It.IsNotNull<string>()))
                .Returns<string>(u => credentials);
        }


    }
}