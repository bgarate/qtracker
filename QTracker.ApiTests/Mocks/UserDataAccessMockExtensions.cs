﻿using System;
using Microsoft.EntityFrameworkCore;
using Moq;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.ApiTests.Mocks
{
    public static class UserDataAccessMockExtensions
    {
        private static readonly QTrackerContext dummyContext = new QTrackerContext(new DbContextOptions<QTrackerContext>());
        public static Mock<UserDataAccess> GetMock() => new Mock<UserDataAccess>(dummyContext);
        public static void AddUserReturnsUser(this Mock<UserDataAccess> mock)
        {
            mock.Setup(m => m.AddUser(It.IsNotNull<User>()))
                .Returns<User>(u =>
                {
                    User user = new User(u.Name) {UserId = Guid.NewGuid()};
                    return user;
                });
        }
    }
}