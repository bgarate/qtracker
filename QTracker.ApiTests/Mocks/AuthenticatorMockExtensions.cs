﻿using System;
using Moq;
using QTracker.BusinessLogic;
using QTracker.Entities;

namespace QTracker.ApiTests.Mocks
{
    public static class AuthenticatorMockExtensions
    {
        public static Mock<Authenticator> GetMock(IDateTimeProvider dateTimeProvider) => new Mock<Authenticator>(dateTimeProvider);
        public static void GenerateCredentialsReturnsCredentials(this Mock<Authenticator> mock)
        {
            mock.Setup(m => m.GenerateCredentials(It.IsNotNull<UserCreationData>()))
                .Returns<UserCreationData>(u => new Credentials(u.Login, "saltedPassword"));
        }

        public static void IsLoggedReturnsTrueFor(this Mock<Authenticator> mock,LoggedUser loggedUser)
        {
            mock.Setup(m => m.IsLogged(loggedUser)).Returns<LoggedUser>(l => true);
        }

        public static void LogInValidatesFor(this Mock<Authenticator> mock, string pasword)
        {
            mock.Setup(m => m.LogIn(It.IsNotNull<Credentials>(), It.Is<string>(s => s == pasword)))
                .Returns<Credentials, string>((c, p) =>
                    new LoggedUser(c.UserId, Token.Generate(), DateTime.Now, DateTime.Now.AddDays(1)));
        }

        public static void LogInValidatesFor(this Mock<Authenticator> mock, string pasword, LoggedUser returnedLoggedUser)
        {
            mock.Setup(m => m.LogIn(It.IsNotNull<Credentials>(), It.Is<string>(s => s == pasword)))
                .Returns<Credentials, string>((c, p) => returnedLoggedUser);
        }
    }
}