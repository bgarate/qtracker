﻿using System;
using System.Collections.Generic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class CustomerManager
    {
        public CustomerManager(CustomerDataAccess customerDataAccess)
        {
            CustomerDataAccess = customerDataAccess;
        }

        private CustomerDataAccess CustomerDataAccess { get; }

        public List<Customer> GetCustomers()
        {
            return CustomerDataAccess.GetCustomers();
        }

        public Customer GetCustomer(string code)
        {
            return CustomerDataAccess.GetCustomer(code);
        }
        public Customer GetCustomer(Guid customerId)
        {
            return CustomerDataAccess.GetCustomer(customerId);
        }

        public Customer AddCustomer(Customer customer)
        {
            return CustomerDataAccess.AddCustomer(customer);
        }
    }
}