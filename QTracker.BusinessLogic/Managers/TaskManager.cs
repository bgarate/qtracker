﻿using System;
using System.Collections.Generic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class TaskManager
    {
        public TaskManager(TaskDataAccess taskDataAccess)
        {
            TaskDataAccess = taskDataAccess;
        }

        private TaskDataAccess TaskDataAccess { get; }

        public Task GetTask(Guid taskId)
        {
            return TaskDataAccess.GetTask(taskId, true);
        }
        public Task AddTask(Task task)
        {
            return TaskDataAccess.AddTask(task);
        }
    }
}