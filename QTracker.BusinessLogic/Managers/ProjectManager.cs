﻿using System;
using System.Collections.Generic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class ProjectManager
    {
        public ProjectManager(ProjectDataAccess projectDataAccess)
        {
            ProjectDataAccess = projectDataAccess;
        }

        private ProjectDataAccess ProjectDataAccess { get; }

        public Project GetProject(Guid projectId)
        {
            return ProjectDataAccess.GetProject(projectId);
        }
        public Project AddProject(Project project)
        {
            return ProjectDataAccess.AddProject(project);
        }
    }
}