﻿using System;
using System.Collections.Generic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class TimeTrackManager
    {
        public TimeTrackManager(TimeTrackDataAccess timeTrackDataAccess)
        {
            TimeTrackDataAccess = timeTrackDataAccess;
        }

        private TimeTrackDataAccess TimeTrackDataAccess { get; }

        public TimeTrack AddTimeTrack(TimeTrack timeTrack)
        {
            return TimeTrackDataAccess.AddTimeTrack(timeTrack);
        }
        public TimeTrack GetTimeTrack(Guid timeTrackId)
        {
            return TimeTrackDataAccess.GetTimeTrack(timeTrackId);
        }
    }
}