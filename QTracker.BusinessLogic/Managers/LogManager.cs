﻿using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class LogManager
    {
        public LogManager(Authenticator authenticator, AuthDataAccess authDataAccess)
        {
            Authenticator = authenticator;
            AuthDataAccess = authDataAccess;
        }

        private Authenticator Authenticator { get; }
        private AuthDataAccess AuthDataAccess { get; }

        public Token LogIn(LogInformation info)
        {
            
            Credentials credentials = AuthDataAccess.GetCredentials(info.User);

            if(credentials == null)
                return Token.InvalidToken;

            LoggedUser loggedUser = Authenticator.LogIn(credentials, info.Password);

            if (!Authenticator.IsLogged(loggedUser))
                return Token.InvalidToken;

            AuthDataAccess.LogUser(loggedUser);

            return loggedUser.Token;

        }


    }
}
