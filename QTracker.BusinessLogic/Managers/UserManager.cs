﻿using System;
using System.Collections.Generic;
using QTracker.DataAccess;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class UserManager
    {
        public UserManager(UserDataAccess userDataAccess, AuthDataAccess authDataAccess, Authenticator authenticator)
        {
            UserDataAccess = userDataAccess;
            AuthDataAccess = authDataAccess;
            Authenticator = authenticator;
        }

        private UserDataAccess UserDataAccess { get; }
        private AuthDataAccess AuthDataAccess { get; }
        private Authenticator Authenticator { get; }

        public User GetUser(Guid userId)
        {
            return UserDataAccess.GetUser(userId);
        }

        public User GetUser(string login)
        {
            Credentials credentials = AuthDataAccess.GetCredentials(login);

            if (credentials == null)
                return null;

            return UserDataAccess.GetUser(credentials.UserId);
        }

        public User AddUser(UserCreationData userCreation)
        {
            Credentials existentCredentials = AuthDataAccess.GetCredentials(userCreation.Login);

            if (existentCredentials != null)
                throw new AlreadyExists();

            Credentials credentials = Authenticator.GenerateCredentials(userCreation);
            User user = new User(userCreation.Name);
            user.AddRole(Role.UserRole);

            user = UserDataAccess.AddUser(user);
            AuthDataAccess.AddCredentials(user, credentials);

            return user;
        }

        public CustomerUserAccess Accesses(Guid userId, CustomerAccessInformation accessInformation)
        {
            return UserDataAccess.Accesses(userId, accessInformation.CustomerId, accessInformation.Access, false);
        }

        public CustomerUserAccess ChangeAccesses(Guid userId, CustomerAccessInformation accessInformation)
        {
            return UserDataAccess.Accesses(userId, accessInformation.CustomerId, accessInformation.Access, true);
        }

        public List<Customer> GetCustomers(DateTime start, DateTime end, Guid userId)
        {
            return UserDataAccess.GetCustomers(start, end, userId);
        }
    }
}