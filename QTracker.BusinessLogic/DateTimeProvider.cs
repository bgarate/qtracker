﻿using System;

namespace QTracker.BusinessLogic
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetNow() => DateTime.Now;
    }
}