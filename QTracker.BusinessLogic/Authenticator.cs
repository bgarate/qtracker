﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using QTracker.Entities;

namespace QTracker.BusinessLogic
{
    public class Authenticator
    {
        public Authenticator(IDateTimeProvider dateTimeProvider)
        {
            DateTimeProvider = dateTimeProvider;
        }

        private IDateTimeProvider DateTimeProvider { get; }
        
        public const int SALT_SIZE_IN_BITS = 128;
        public const int PASSWORD_SIZE_IN_BITS = 512;
        public const int LOGGED_TIME_IN_MINUTES = 60;
        public const int HASH_ITERATIONS = 1024;

        public const char SALTED_PASSWORD_SEPARATOR = '.';

        public virtual LoggedUser LogIn(Credentials credentials, string sentPassword)
        {
           
            if (credentials == null)
                return LoggedUser.NotLogged(Guid.Empty);

            (int iterations, string base64Salt, string base64Hash) = GetSaltedPasswordParts(credentials.SaltedPassword);

            byte[] salt = Convert.FromBase64String(base64Salt);
            
            string base64SentHash = GetSaltedPassword(sentPassword, salt, iterations);

            if(base64SentHash != base64Hash)
                return LoggedUser.NotLogged(credentials.UserId);
            
            Token token = Token.Generate();

            LoggedUser loggedUser = new LoggedUser(credentials.UserId, token, DateTimeProvider.GetNow(),
                DateTimeProvider.GetNow().AddMinutes(LOGGED_TIME_IN_MINUTES));

            return loggedUser;
        }

        private static string GetSaltedPassword(string sentPassword, byte[] salt, int iterations)
        {
            byte[] sentHash = KeyDerivation.Pbkdf2(sentPassword, salt, KeyDerivationPrf.HMACSHA512, iterations,
                PASSWORD_SIZE_IN_BITS / Consts.BITS_IN_BYTE);

            string base64SentHash = Convert.ToBase64String(sentHash);
            return base64SentHash;
        }

        private (int iterations, string base64Salt, string base64Hash) GetSaltedPasswordParts(string saltedPassword)
        {
            string[] parts = saltedPassword.Split(SALTED_PASSWORD_SEPARATOR);
            return (int.Parse(parts[0]), parts[1], parts[2]);
        }

        private string JoinSaltedPassword(int iterations, string base64Salt, string base64Hash)
        {
            return $"{iterations}{SALTED_PASSWORD_SEPARATOR}{base64Salt}{SALTED_PASSWORD_SEPARATOR}{base64Hash}";
        }

        private byte[] GetSalt(int sizeInBits)
        {
            byte[] salt = new byte[sizeInBits / Consts.BITS_IN_BYTE];
            using (RandomNumberGenerator rnd = RandomNumberGenerator.Create())
            {
                rnd.GetBytes(salt);
            }

            return salt;
        }

        public virtual bool IsLogged(LoggedUser loggedUser)
        {
            return loggedUser != null && loggedUser.UserId != Guid.Empty && loggedUser.Token.IsValid() && 
                   loggedUser.LoggedOn.HasValue && loggedUser.LoggedOn <= DateTimeProvider.GetNow() &&
                   loggedUser.LoggedUntil.GetValueOrDefault() > DateTimeProvider.GetNow();
        }

        public virtual Credentials GenerateCredentials(UserCreationData userCreation)
        {
            byte[] salt = GetSalt(SALT_SIZE_IN_BITS);
            string saltedPassword = GetSaltedPassword(userCreation.Password, salt, HASH_ITERATIONS);
            string base64Salt = Convert.ToBase64String(salt);

            string joinedHash = JoinSaltedPassword(HASH_ITERATIONS, base64Salt, saltedPassword);
            Credentials credentials = new Credentials(userCreation.Login, joinedHash);

            return credentials;
        }
    }
}