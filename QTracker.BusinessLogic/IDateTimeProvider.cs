﻿using System;

namespace QTracker.BusinessLogic
{
    public interface IDateTimeProvider
    {
        DateTime GetNow();
    }
}